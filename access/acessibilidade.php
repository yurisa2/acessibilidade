<?php
session_start();

$caminho ="../";
?>

<?php session_start(); ?><!DOCTYPE html>
<html>
<?php
	$pagina = "Acess";
?>
<!-- Incluindo o head padrão no documento -->
<?php
	include $caminho."includes/head.php";
 ?>
 <!-- Término da inclusão do head padrão no documento -->

 <body>

<!-- Incluindo o navbar padrão no documento -->
<?php
    include $caminho."includes/nav.php";
 ?>
<!-- Término da inclusão do navbar padrão no documento -->
 
         <h3>Página de Acessibilidade</h3>
            <article>

            <div class="clear"></div>

            <article class="span12">
              <div class="inner-1">
                <h4>Acessibilidade de nosso site</h4>
                <p>De acordo com o <a href="http://www.w3c.br/">W3C</a> (World Wide Web Consortium), Acessibilidade na Web significa garantir que todas as pessoas, incluindo pessoas com deficiência, possam utilizar a Web. Mais especificamente, significa permitir que pessoas com deficiência consigam perceber, compreender, navegar, interagir e contribuir com a Web. Uma Web acessível beneficia a todos, incluindo pessoas idosas, pessoas com pouca habilidade em utilizar a Web, aqueles com uma conexão mais lenta, entre outros.</p>
                <p>Em nosso site, seguimos as recomendações de acessibilidade dos documentos <a href="http://www.w3.org/Translations/WCAG20-pt-br/ ">WCAG 2.0</a> (Web Content Accessibility Guidelines – internacional) e <a href="http://emag.governoeletronico.gov.br/">eMAG 3.1</a> (Modelo de Acessibilidade em Governo Eletrônico – nacional).</p>

                <p>No topo das páginas de nosso site, disponibilizamos uma barra de acessibilidade, que contém:</p>
                <ul>
                  <li><strong>Atalhos de teclado:</strong> permitem ir diretamente a um bloco do site, facilitando a navegação para quem utiliza o teclado, como pessoas cegas e com certas limitações físicas;</li>
                  <li><strong>Página de acessibilidade:</strong> apresenta informações sobre a acessibilidade do site, recursos oferecidos e testes realizados;</li>
                  <li><strong>Alto contraste:</strong> passa a apresentar a página com um contraste otimizado, facilitando a navegação para quem tem baixa visão;</li>
                </ul>

                <h5>Utilizando os atalhos</h5>
                <p>Em nosso site, disponibilizamos atalhos de teclado:</p>
                <ul>
                  <li>h: Ir para o conteúdo principal da HOME</li>
                  <li>z: Ir para o Tutoriais</li>
                  <li>x: Ir para Aprenda</li>
                  <li>c: Ir para Dicas e Seguranca</li>
                  <li>v: Ir para Distros</li>
                  <li>b: Ir para Quem Somos</li>
                </ul>

                <h5>Ampliando o texto no navegador</h5>
                <ul>
                  <li>Pressione <strong>“Ctrl”</strong> + <strong>“+”</strong> (sinal de mais) para aumentar a fonte do texto;</li>
                  <li>Pressione <strong>“Ctrl”</strong> + <strong>“-”</strong> (sinal de menos) para diminuir a fonte do texto;</li>
                  <li>Pressione <strong>“Ctrl”</strong> + <strong>“0”</strong> (zero) para retornar ao tamanho padrão da fonte.</li>
                </ul>

                <p>No Mac OS, substitua o “Ctrl’ pela tecla “Command”.</p>
              </div>
            </article>

            <div class="clear"></div>

    

    <!-- Incluindo o rodapé padrão no documento -->
    <?php
    	include $caminho."includes/footer.php";
    ?>
    <!-- Término da inclusão do rodapé padrão no documento -->

    <!-- Incluindo os scripts de JQuery e bootstrap.js -->
    <?php
    	include $caminho."includes/scripts.php";
     ?>
    <!-- Término da inclusão do JQuery e bootstrap.min -->
  </body>
</html>
