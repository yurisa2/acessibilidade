<?php session_start(); ?>
<?php
	$caminho="../";
	$pagina = 'Quem somos';

	$alunos = array("Davino Junior", "Alberto Aguiar", "Alexsander Ribeiro", "Rodrigo Garcia", "Yuri Sá");
	$davino = array("Davino Junior", "2650831523013");
	$alexsander = array("Alexsander Ribeiro", "2650831523021");
	$rodrigo = array("Rodrigo Garcia", "2650831523017");
	$yuri = array("Yuri Sá", "2650831523026");
?>

<!DOCTYPE html>
<html>

<!-- Incluindo o head padrão no documento -->
<?php
	include $caminho."includes/head.php";
 ?>
 <!-- Término da inclusão do head padrão no documento -->

 <body>

<!-- Incluindo o navbar padrão no documento -->
<?php
    include $caminho."includes/nav.php";
 ?>
<!-- Término da inclusão do navbar padrão no documento -->

<!-- Inicio do conteúdo -->
	<section class="container alunos">
		<!-- Aluno 1 -->
		<div class="row">
			<div class=" col-lg-3 col-md-3 col-sm-12 col-xs-12">
				<img class="center-block img-responsive" src="<?php echo $caminho;?>imgs/imgs-quem-somos/yoda.png" alt="Líder da equipe - Davino Gonçalves">
			</div>
			<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
				<h2><?php echo $davino[0]; ?></h2>
				<h3>RA: <?php echo $davino[1]; ?></h3>
				<p>Minicurrículo: Estudante do 3º Semestre de Sistemas para Internet da FATEC São Roque. Músico, Analista de sistemas e Empresário.</p>
				<p><a href="../aprenda" class="btn btn-aprenda">Aprenda</a></p>
			</div>
		</div>



		<!-- Aluno 3 -->
		<div class="row">
			<div class=" col-lg-3 col-md-3 col-sm-12 col-xs-12">
				<img class="center-block img-responsive" src="<?php echo $caminho;?>imgs/imgs-quem-somos/lecsander.png" alt="Alexsander Caldeirão do Huck - ImageMaster">
			</div>
			<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
				<h2><?php echo $alexsander[0]; ?></h2>
				<h3>RA: <?php echo $alexsander[1]; ?></h3>
				<p>Minicurrículo: Estudante do 3º Semestre de Sistemas para Internet da FATEC São Roque, Design Gráfico, Controlador de Tráfego, Apaixonado pela Interneta!</p>
				<p><a href="../tutoriais" class="btn btn-tutoriais">Tutoriais</a></p>
			</div>
		</div>

		<!-- Aluno 4 -->
		<div class="row">
			<div class=" col-lg-3 col-md-3 col-sm-12 col-xs-12">
				<img class="center-block img-responsive" src="<?php echo $caminho;?>imgs/imgs-quem-somos/rgl.png" alt="Rei do LinkedIN">
			</div>
			<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
				<h2><?php echo $rodrigo[0]; ?></h2>
				<h3>RA: <?php echo $rodrigo[1]; ?></h3>
				<p>Minicurrículo: Químico Industrial com experiência em mineração e metalurgia.</p>
				<p><a href="../dicas-seguranca" class="btn btn-dicas-seguranca">Dicas & Segurança</a></p>
			</div>
		</div>

		<!-- Aluno 5 -->
		<div class="row">
			<div class=" col-lg-3 col-md-3 col-sm-12 col-xs-12">
				<img class="center-block img-responsive" src="<?php echo $caminho;?>imgs/imgs-quem-somos/yuri.png" alt="Mecânico de PC - Aquele por trás da tela preta">
			</div>
			<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
				<h2><?php echo $yuri[0]; ?></h2>
				<h3>RA: <?php echo $yuri[1]; ?></h3>
				<p>Minicurrículo: Pensativo, vegetariano, amante da natureza, além de código gosta bastante de crochê e passar as tardes lendo no parque. Gosta de MPB e Chorinho. <em>Wait...Oh Wait...</em></p>
				<p><a href="../distribuicoes" class="btn btn-distro">Distros</a></p>
			</div>
		</div>
	</section>
<!-- Término do conteúdo -->



<!-- Incluindo o rodapé padrão no documento -->
<?php
	include $caminho."includes/footer.php";
 ?>
<!-- Término da inclusão do rodapé padrão no documento -->


<!-- Incluindo os scripts de JQuery e bootstrap.js -->
<?php
	include $caminho."includes/scripts.php";
 ?>
<!-- Término da inclusão do JQuery e bootstrap.min -->

 </body>
