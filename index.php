<?php session_start(); ?>
<?php
    $caminho = "";
    $pagina = "Inicio";
?>

<!DOCTYPE html>
<html>
<?php
    $pagina = "Inicio";
?>
<!-- Incluindo o head padrão no documento -->
<?php
	include $caminho."includes/head.php";
 ?>
 <!-- Término da inclusão do head padrão no documento -->

 <body>

<!-- Incluindo o navbar padrão no documento -->
<?php
    include $caminho."includes/nav.php";
 ?>
<!-- Término da inclusão do navbar padrão no documento -->

<!-- Inicio do conteúdo -->
<div class="container-fluid inicio">

        <section class="col-md-4 col-sm-6 col-xs-12">
            <div class="card">
            <div class="imagem-index">
                <img class="center-block img-responsive" src="<?php echo $caminho; ?>imgs/tutoriais-index.png" title="Imagem de uma área de trabalho com uma janela aberta contendo arquivos" alt="Imagem de uma área de trabalho com uma janela aberta contendo arquivos">
            </div>
                <h4 class="text-center titulo-tutoriais">05 distros Linux para usuários iniciantes</h4>
                <p>Acompanhe o passo a passo de algumas das diversas funcionalidades do Linux para todos, inclusive iniciantes...</p>
                <a href="<?php echo $caminho; ?>tutoriais/index.php" class="btn center-block btn-tutoriais">Leia mais</a>
            </div>
        </section>

        <section class="col-md-4 col-sm-6 col-xs-12">
            <div class="card">
            <div class="imagem-index">
                <img class="center-block img-responsive" src="<?php echo $caminho; ?>imgs/aprenda-index.png" title="Imagem do pinguim fazendo anotações" alt="imagem do pinguim fazendo anotações">
            </div>
                <h4 class="text-center titulo-aprenda">Aprenda A Procurar Softwares Maliciosos</h4>
                <p>“Nenhum sistema é 100% seguro”… e os sistemas Linux não poderiam ficar fora disso. Existem diversas maneiras... </p>
                <a href="<?php echo $caminho; ?>aprenda/index.php" class="btn center-block btn-aprenda">Leia mais</a>
            </div>
        </section>

        <section class="col-md-4 col-sm-6 col-xs-12">
            <div class="card">
            <div class="imagem-index">
                <img class="center-block img-responsive" src="<?php echo $caminho; ?>imgs/dicas-index.png" title="Pinguim do Linux sentado à frente de um cavalo de tróia e de dentro do cavalo saindo vários vírus." alt="Pinguim do Linux sentado à frente de um cavalo de tróia e de dentro do cavalo saindo vários vírus.">
            </div>
                <h4 class="text-center titulo-dicas-seguranca">Como manter seu Sistema Atualizado...</h4>
                <p>Mesmo sendo uma versão LTS (suporte estendido), quanto mais recente for o lançamento, mais propenso a erros estará... </p>
                <a href="<?php echo $caminho; ?>dicas-seguranca/index.php" class="btn center-block btn-dicas-seguranca">Leia mais</a>
            </div>
        </section>

        <section class="col-md-4 col-sm-6 col-xs-12">
            <div class="card">
            <div class="imagem-index">
                <img class="center-block img-responsive" src="imgs/distros-index.png" title="Vários moniores mostrando as distribuições Linux com o texo Muitas distribuições, mesmo kenel, mesma liberdade" alt="Vários moniores mostrando as distribuições Linux com o texo Muitas distribuições, mesmo kenel, mesma liberdade">
            </div>
                <h4 class="text-center titulo-distro">Distribuições</h4>
                <p>Você ficará surpreso com a quantidade de distribuições Linux para sua casa ou empresa. Vem comigo!!!</p>
                <a href="<?php echo $caminho; ?>distribuicoes/index.php" class="btn center-block btn-distro">Leia mais</a>
            </div>
        </section>

        <section class="col-md-4 col-sm-6 col-xs-12">
            <div class="card">
            <div class="imagem-index">
                <img class="center-block img-responsive" src="imgs/quemsomos-index.png" title="Cinco homens em tom de azul com as mãos nos bolsos posando para uma foto corporativa" alt="Cinco homens em tom de azul com as mãos nos bolsos posando para uma foto corporativa">
            </div>
                <h4 class="text-center">Quem Somos</h4>
                <p>Seção que mostra um pouco sobre cada um dos desenvolvedore do portal e suas preferências em geral...</p>
                <a href="<?php echo $caminho; ?>quem-somos/" class="btn center-block btn-primary">Leia mais</a>
            </div>
        </section>
</div>
<!-- Término do conteúdo -->
<!-- Newsletter -->
<div class="container inicio">
    <section class="newsletter">
        <div class="body-newsletter">
            <h2>Junte-se a nós!</h2>
            <p>Se inscreva e ganhe o convite para a sua área restrita!</p>

            <form action="http://daray.com.br" method="post">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-envelope"></i>
                    </span>
                    <input class="form-control" type="email" placeholder="seu@email.com" required>
                </div>
                <input type="submit" value="Inscrever-se" class="btn btn-large btn-primary" />
            </form>
        </div>
    </section>
</div>
<!-- Término da newsletter -->



<!-- Incluindo o rodapé padrão no documento -->
<?php
	include $caminho."includes/footer.php";
?>
<!-- Término da inclusão do rodapé padrão no documento -->

<!-- Incluindo os scripts de JQuery e bootstrap.js -->
<?php
	include $caminho."includes/scripts.php";
 ?>
<!-- Término da inclusão do JQuery e bootstrap.min -->

 </body>
</html>
