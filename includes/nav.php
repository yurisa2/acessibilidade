<?php
if ($pagina == "Inicio" || $pagina == "Quem somos" || $pagina == "Login"):?>
<header class="cabecalho">
	<img class="img-responsive center-block" src="<?php echo $caminho?>imgs/logo.png" alt="Logo Portal L.I.N.U.X com lindos pinguins ao lado da frase PORTAL LINUX" title="Logo versão grande do Portal .L.I.N.U.X">
</header>
<?php else: ?>
<header class="container cabecalho2">
    <div class="col-md-4 col-sm-4">
        <img class="img-responsive center-block logo-pequeno" src="<?php echo $caminho;?>imgs/logo-pequeno.png" alt="Logo Portal L.I.N.U.X com lindos pinguins em cima da frase PORTAL LINUX" title="Logo versão reduzida do Portal L.I.N.U.X">
    </div>

    <div class="col-md-8 col-sm-8 text-right">
        <a href="https://facebook.com" target="_blank"><span class="fa fa-facebook"></span></a>
        <a href="https://twitter.com" target="_blank"><span class="fa fa-twitter"></span></a>
        <a href="https://googleplus.com" target="_blank"><span class="fa fa-google-plus"></span></a>
        <a href="https://linkedin.com" target="_blank"><span class="fa fa-linkedin"></span></a>
        <a href="mailto:contato@portallinux.com.br">contato@portallinux.com.br</a>
    </div>
</header>
<?php endif; ?>
<nav class="navbar">
    <div class="container navbarDefault">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar" style="color: #111 !important;">
            	<span class="sr-only">Toggle navigation</span>
            	<i class="fa fa-bars" aria-hidden="true"></i>
            </button>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-left">
            	<?php if ($pagina == 'Inicio') { ?><li>
								<a class="inicio ativo-inicio" href="<?php echo $caminho;?>index.php">
								Inicio
							</a></li>
                <?php } else { ?><li><a class="inicio" href="<?php echo $caminho;?>index.php" accesskey="h" >Inicio

									<?php
									if($_SESSION["Accessivel"]==1) echo ' Alt + H';
									?>

								</a></li><?php } ?>
								<?php
								if($_SESSION["Accessivel"]==1) echo '<br><br><br>';
								?>
                <?php if ($pagina == 'Tutoriais') { ?><li>
									<a class="tutoriais ativo-tutoriais"  href="<?php echo $caminho;?>tutoriais/" accesskey="z" >Tutoriais
								</a></li>
                <?php } else { ?><li>
									<a class="tutoriais" href="<?php echo $caminho;?>tutoriais/" accesskey="z" >Tutoriais
										<?php
										if($_SESSION["Accessivel"]==1) echo ' Alt + Z';
										?>

								</a></li><?php } ?>
								<?php
								if($_SESSION["Accessivel"]==1) echo '<br><br><br>';
								?>
                <?php if ($pagina == 'Aprenda') { ?><li>
									<a class="aprenda ativo-aprenda" href="<?php echo $caminho;?>aprenda/" accesskey="x">Aprenda
								</a></li>
                <?php } else { ?><li>
									<a class="aprenda" href="<?php echo $caminho;?>aprenda/" accesskey="x">Aprenda
										<?php
										if($_SESSION["Accessivel"]==1) echo ' Alt + X';
										?>

								</a></li><?php } ?>
								<?php
								if($_SESSION["Accessivel"]==1) echo '<br><br><br>';
								?>
                <?php if ($pagina == 'Dicas & Segurança') { ?><li>
									<a class="dicas-seguranca ativo-dicas" href="<?php echo $caminho;?>dicas-seguranca/" accesskey="c">Dicas & Segurança
								</a></li>
                <?php } else { ?><li>
									<a class="dicas-seguranca" href="<?php echo $caminho;?>dicas-seguranca/" accesskey="c">Dicas & Segurança

										<?php
										if($_SESSION["Accessivel"]==1) echo ' Alt + C';
										?>

								</a></li><?php } ?>
								<?php
								if($_SESSION["Accessivel"]==1) echo '<br><br><br>';
								?>
                <?php if ($pagina == 'Distros') { ?><li>
									<a class="distro" href="<?php echo $caminho;?>distribuicoes/" accesskey="v">Distro
									</a></li>

                <?php } else { ?><li>
									<a class="distro" href="<?php echo $caminho;?>distribuicoes/" accesskey="v">Distro
										<?php
										if($_SESSION["Accessivel"]==1) echo ' Alt + V';
										?>

								</a></li><?php } ?>
								<?php
								if($_SESSION["Accessivel"]==1) echo '<br><br><br>';
								?>
                <?php if ($pagina == 'Quem somos') { ?><li>
									<a class="quem-somos ativo-somos" href="<?php echo $caminho;?>quem-somos"  accesskey="b">Quem Somos
								</a></li>
                <?php } else { ?><li><a class="quem-somos" href="<?php echo $caminho;?>quem-somos" accesskey="b">Quem Somos
									<?php
									if($_SESSION["Accessivel"]==1) echo ' Alt + B';
									?>


								</a></li><?php } ?>

								<?php
								if($_SESSION["Accessivel"]==1) echo '<br><br><br>';
								?>

            </ul>
            <ul class="nav navbar-nav navbar-right">
							<!--<br>
							<br>
							<br>-->
              <li><a class="login" href="<?php echo $caminho;?>access/">
								<?php
								if($_SESSION["Accessivel"]==1) echo 'Versão Normal <i class="fa fa-male" aria-hidden="true"></i>';
								else echo 'Versão Acessível <i class="fa fa-wheelchair-alt">';
								?> </i></a></li>
            </ul>
					</ul>

					<ul class="nav navbar-nav navbar-right">
						<div id="google_translate_element"></div><script type="text/javascript">
					function googleTranslateElementInit() {
					  new google.translate.TranslateElement({pageLanguage: 'pt', includedLanguages: 'en,es,pt', layout: google.translate.TranslateElement.InlineLayout.HORIZONTAL}, 'google_translate_element');
					}
					</script><script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
					        					</ul>


          </div><!--/.nav-collapse -->
        </div><!--/.container -->
      </nav>
