<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
<?php
include $caminho.'includes/includes.php';
include $caminho.'includes/access.php';

?>
<?php if (isset($post)):?>
    <title>L.I.N.U.X - <?php echo $post?></title>
<?php else: ?>
    <title>L.I.N.U.X - <?php echo $pagina?></title>
<?php endif; ?>
    <link rel="stylesheet" href="<?php echo $caminho?>css/bootstrap.min.css">
    <?php
    if($_SESSION['Accessivel'] == true) echo '<link rel="stylesheet" href="'.$caminho.'css/style_access.css">';
    else echo '<link rel="stylesheet" href="'.$caminho.'css/style.css">';

    ?>
    <link rel="stylesheet" href="<?php echo $caminho?>css/font-awesome.min.css">
    <link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico" />

</head>
