<footer class="rodape">
	<div class="cimaRodape container">
		<div class="col-md-4">
			<h2>Social</h2>
			<div class="redes-sociais rede-facebook"><a target="new" href="https://www.facebook.com" class="fa fa-facebook"></a></div>
			<div class="redes-sociais rede-twitter"><a target="new" href="https://www.twitter.com" class="fa fa-twitter"></a></div>
			<div class="redes-sociais rede-gplus"><a target="new" href="https://www.plus.google.com" class="fa fa-google-plus"></a></div>
			<div class="redes-sociais rede-linkedin"><a target="new" href="https://www.linkedin.com" class="fa fa-linkedin-square"></a></div>
		</div>

		<div class="col-md-4">
			<h2>Mapa</h2>
			<div class="list-group">
				<a class="list-group-item" href="<?php echo $caminho;?>index.php"><i class="fa fa-home fa-fw"></i>&nbsp; Inicio</a>
				
				<a class="list-group-item" href="<?php echo $caminho;?>tutoriais/"><i class="fa fa-pencil fa-fw"></i>&nbsp; Tutoriais</a>
				<a class="list-group-item" href="<?php echo $caminho;?>aprenda/"><i class="fa fa-leanpub fa-fw"></i>&nbsp; Aprenda</a>
				<a class="list-group-item" href="<?php echo $caminho;?>dicas-seguranca"><i class="fa fa-user-secret fa-fw"></i>&nbsp; Dicas & Segurança</a>
				<a class="list-group-item" href="<?php echo $caminho;?>distribuicoes/"><i class="fa fa-linux fa-fw"></i>&nbsp; Distro</a>
				<a class="list-group-item" href="<?php echo $caminho;?>quem-somos/"><i class="fa fa-users fa-fw"></i>&nbsp; Quem somos</a>
<!--			<a class="list-group-item" href="<?php echo $caminho;?>login/"><i class="glyphicon glyphicon-log-in fa-fw"></i>&nbsp; Login</a>-->
			</div>
			
		</div>

		<div class="col-md-4">
			<h2>Contato</h2>
			<form>
				<div class="form-group">
			    	<input type="text" class="form-control" placeholder="Nome" required>
			  	</div>
				<div class="form-group">
					<input type="email" class="form-control" placeholder="E-mail" required>
				</div>
				<div class="form-group">
					<input type="text" class="form-control" placeholder="Assunto" required>
				</div>

				<textarea class="form-control" rows="3" placeholder="Digite aqui a sua mensagem"></textarea>
				<button type="submit" class="btn btn-enviar">Enviar</button>
			</form>
		</div>

	</div>
	<div class="copyright text-center">
		<small>Developed by Daray Web</small>
	</div>
</footer>