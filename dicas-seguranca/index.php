<?php
session_start();

$caminho ="../";
?>

<?php session_start(); ?><!DOCTYPE html>
<html>
<?php
	$pagina = "Dicas & Segurança";
?>
<!-- Incluindo o head padrão no documento -->
<?php
	include $caminho."includes/head.php";
 ?>
 <!-- Término da inclusão do head padrão no documento -->

 <body>

<!-- Incluindo o navbar padrão no documento -->
<?php
  include $caminho."includes/nav.php";
 ?>
<!-- Término da inclusão do navbar padrão no documento -->


<!-- Inicio do conteúdo -->
     <h1><b>Dicas & Segurança</b>
     </h1>
     
     <div class="container">
<section class="col-md-10 dicas">
    <div class="mant1 row">
    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">

            <div class="foto">

			<img class="center-block img-responsive" src="<?php echo $caminho;?>imgs/imgs-dicas/data1.jpg" title="Data do post" alt="Data do post">
            </div>
		</div>

        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
            <a class="dic1" href="index.php"><b>Manter o sistema atualizado</b></a>
            
            <p>Todo software novo, vem com bugs, que    precisam ser corrigidos.</p>
                
               <p>Mesmo sendo uma versão LTS (suporte estendido), quanto mais recente foi o lançamento, mais propenso a conter erros ele estará. </p>
                
               <p>Algumas atualizações, não chegam a 100Kb — ou seja, não faz sentido algum deixar para amanhã, depois ou “daqui a pouco”. Nos primeiros meses, após o lançamento do sistema operacional, as atualizações são sempre tarefas prioritárias.</p>
                
               <p>Você não precisa interromper o que está fazendo para executar uma atualização. Se preferir, pode minimizar a janela indicadora de progresso da atualização e esquecer o assunto. O sistema avisará quando terminar a tarefa.</p>
                
               <p>Opcionalmente, durante o processo de atualização, você pode acompanhar o que está ocorrendo, clicando em Detalhes, dentro da janela indicadora.</p>
     
            <a class="btn btn-info btn-cor-dicas" href="<?php echo $caminho;?>quem-somos/" role="button"><b>Sobre Yuri Sá...</b></a>
		</div>
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
            <div class="foto1">
            <img class="center-block img-responsive" src="<?php echo $caminho;?>imgs/imgs-dicas/sistema.jpg" title="Pinguim do Linux sentado" alt="Pinguim do Linux sentado">
            </div>
        </div>
    </div>


	<div class="mant2 row">
		<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                <div class="foto">
				<img class="center-block img-responsive" src="<?php echo $caminho;?>imgs/imgs-dicas/data1.jpg" title="Data do post" alt="Data do post">
                </div>
                </div>

	       <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">

            <a class="dic1" href="index.php"><b>Firewall</b></a>

	            <p>Com uma firewall juntamente com um Antivírus você consegue proteger o seu computador de:</p>

                <p>• Vírus, também denominados de ‘worms’, que se espalham de computador em computador por toda a Internet e depois dão o controlo da sua máquina a outras pessoas mal intencionadas.</p>

                <p>• Hackers que desejem entrar no seu computador para tomar conta dele e fazer ataques disfarçado ou roubar dados pessoais que tenha no disco rígido.</p>

               <p>• Bloqueia o tráfego de saída para não deixar que determinados protocolos sejam utilizados para espalhar os vírus que já o podem ter atacado.</p>

               <p>• Firewall pode ser Configurada à sua medida de modo a ser seguro e ao mesmo tempo flexível ao tipo de segurança que pretende.</p>
    
                <a class="btn btn-info btn-cor-dicas" href="<?php echo $caminho;?>quem-somos/" role="button"><b>Sobre Davino...</b></a>
              </div>
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
            <div class="foto1">
            <img class="center-block img-responsive" src="<?php echo $caminho;?>imgs/imgs-dicas/apps.jpg" title="Pinguim do Linux estampando um preservativo" alt="Pinguim do Linux estampando um preservativo">
			</div>
        </div>
    </div>

    <div class="mant3 row">
		        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                <div class="foto">
				<img class="center-block img-responsive" src="<?php echo $caminho;?>imgs/imgs-dicas/data1.jpg" title="Data do post" alt="Data do post">
                </div>
                </div>


	           <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                <a class="dic1" href="index.php"><b>Apps e serviços inicializados automaticamente</b></a>
                   
	            <p>Se a inicialização do Ubuntu está se tornando cada vez mais lenta, os aplicativos executados durante essa etapa podem ser a causa disso. Para resolver, veja como gerenciar a inicialização de aplicativos no Ubuntu.</p>
                   
                <p>Sempre que o Ubuntu inicia, alguns aplicativos e serviços de terceiros também iniciam automaticamente, junto com os serviços e aplicativos principais.</p>
                   
                <p>Como alguns desses aplicativos são adicionados automaticamente à lista de inicialização depois de serem instalados, é importante verificar a lista de tempos em tempos e remover aqueles que você não precisa que sejam executados durante a inicialização.</p>
                   
                <a class="btn btn-info btn-cor-dicas" href="<?php echo $caminho;?>quem-somos/" role="button"><b>Sobre Rodrigo...</b></a>
			</div>
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                <div class="foto1">
                <img class="center-block img-responsive" src="<?php echo $caminho;?>imgs/imgs-dicas/linux.jpg" title="Pinguim do Linux apontando um revólver para o logo do Windows que se encontra com as mãos para cima" alt="Pinguim do Linux apontando um revólver para o logo do Windows que se encontra com as mãos para cima">
			     </div>
        </div>
        </div>


	<div class="mant4 row">
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                <div class="foto">
				<img class="center-block img-responsive" src="<?php echo $caminho;?>imgs/imgs-dicas/data1.jpg" title="Data do post" alt="Data do post">
                </div>
        </div>

			    <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">

	            <a class="dic1" href="index.php"><b>Conhecimento do Sistema</b></a>
                    

	            <p>O sistema operacional GNU/Linux foi desenvolvido por Linus Torvalds, na Finlândia, em 1991. Ele é uma versão do SO Unix que possui código aberto e pode ser escrito e distribuído por qualquer tipo de usuário na internet, por ser um software gratuito (free software), sendo proibido a comercialização do sistema.</p>

                <p>Qualquer pessoa poderá ver o código fonte de um sistema Linux, resolver problemas através de uma lista de discussão online, em que consultores e usuários que trabalham na manutenção do código poderão solucionar, fazer atualizações, etc. Além disso, ele dá suporte a placas, cd-rom e outros dispositivos mais ultrapassados e/ou avançados.</p>

                <p>Das características desse sistema estão a multitarefa, multiusuário, conexão com outros tipos de sistemas operacionais, segurança quanto a proteção de processos executados na memória RAM, não há licença para seu uso, etc.</p>

                <p>O SO Linux é composto pelo kernel e vários programas, que podem ser criados de acordo com as suas distribuições. Cada distribuição linux tem características diferentes e foram criadas para usuários específicos.</p>
                <a class="btn btn-info btn-cor-dicas" href="<?php echo $caminho;?>quem-somos/" role="button"><b>Sobre Rodrigo...</b></a>
		        </div>
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                <div class="foto1">
                <img class="center-block img-responsive" src="<?php echo $caminho;?>imgs/imgs-dicas/know.jpg" 
                title="Pinguim do Linux olhando através da abertura de uma fechadura" title="Pinguim do Linux olhando através da abertura de uma fechadura" alt="Pinguim do Linux olhando através da abertura de uma fechadura">
                </div>
        </div>
    </div>

    <div class="mant5 row">
        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                <div class="foto">
				<img class="center-block img-responsive" src="<?php echo $caminho;?>imgs/imgs-dicas/data1.jpg" title="Data do post"alt="Data do post">
                </div>
        </div>

        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
	            <a class="dic1" href="index.php"><b>Motivos que os Geeks escolhem Linux...</b></a>
                
	            <p>Uma boa razão para usar Linux é ter o prazer de dizer que não usa Windows quando alguém lhe pedir para ir a sua casa consertar seu computador e você não saber o motivo do problema. Você pode dizer que não sabe de Windows e não pode consertar. Sempre encontramos um Windows irrecuperável, pirateado, cheio de vírus, adware, malware e tudo o mais, com dados importantes que o usuário não quer perder, mas, naturalmente, não fez cópia, e um monte de aplicativos instalados. Depois deste quadro sombrio, só nos resta dizer: “Desculpe, amigo, não conheço o Windows, eu uso Linux”.</p>
                <p>Outra razão pela qual nós usamos Linux é o tempo que podemos investir para aprender Informática. Um dia você aprende quatro comandos de terminal, um dia você aprende como acessar o disco, no outro dia como matar um processo, etc. Ao longo dos anos você percebe que você conheceu um monte de coisas, você é capaz de instalar um servidor web e um servidor de email, você sabe quais são as portas e você sabe como bloquear, desbloquear, você aprende o que é um firewall, e você pode começar a dizer que você sabe como o computador funciona por dentro.</p>
            
            <a class="btn btn-info btn-cor-dicas" href="<?php echo $caminho;?>quem-somos/" role="button"> <b>Sobre Alexsander...</b></a>
		</div>
    <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">
                <div class="foto1">
                <img class="center-block img-responsive" src="<?php echo $caminho;?>imgs/imgs-dicas/geek.jpg" title="Pinguim do Linux de braço cruzado e cara de mau com um estilingue em mãos e logo da Microsoft ao fundo com buracos feitos pelas pedras arremessadas pelo pinguim"alt="Pinguim do Linux de braço cruzado e cara de mau com um estilingue em mãos e logo da Microsoft ao fundo com buracos feitos pelas pedras arremessadas pelo pinguim.">
			      </div>
        </div>
    </div>

        </section>

<!-- Término do conteúdo -->
<!-- Inicio do sidebar -->
<aside>
        <div class="col-xs-3 col-md-2">
        <div class="hidden-xs hidden-sm">
           <a class="" href="http://www.congeladosdamayra.com.br" target="_blank"><img class="img-responsive" src="<?php echo $caminho;?>imgs/mayra.png" alt="logo do site Congelados da Mayra"></a>
         <a href="http://www.congeladosdamayra.com.br" target="_blank">Congelados da Mayra, É só descongelar e apreciar.</a>

           <a href="http://sansupersonicprod.com/" target="_blank"><img class="img-responsive" src="<?php echo $caminho;?>imgs/san.png" alt="logo do site Sans Super Sonic Prod."></a>
         <a href="http://sansupersonicprod.com/" target="_blank">Artes Gráficas, Padronização e Vídeo.</a>

           <a href="http://www.arvpaisagismo.eco.br/" target="_blank"><img class="img-responsive" src="<?php echo $caminho;?>imgs/arv.png" alt="logo do site Convelados da Mayra"></a>
         <a href="http://www.arvpaisagismo.eco.br/" target="_blank">Jardins, Paisagismo, Plantio de Grama, etc.</a>

           <a href="http://www.vickysperfumes.com.br/" target="_blank"><img class="img-responsive" src="<?php echo $caminho;?>imgs/vickys.png" alt="logo do site Vicky's Perfumes"></a>
         <a href="http://www.vickysperfumes.com.br/" target="_blank">Perfumes Importados e Acessórios.</a>

           <a href="http://www.martinssociedadeadvogados.com.br/" target="_blank"><img class="img-responsive" src="<?php echo $caminho;?>imgs/martins.png" alt="logo do site Martins Sociedade AdvogadosConvelados da Mayra"></a>
         <a href="http://www.martinssociedadeadvogados.com.br/" target="_blank">Direito da Família, Direito Civil, Direito do Trabalho, Direito das Sucessões.</a>

           <a href="http://cardiocom.com.br/" target="_blank"><img class="img-responsive" src="<?php echo $caminho;?>imgs/cardiocom.png" alt="logo do site Cardiocom"></a>
         <a href="http://cardiocom.com.br/" target="_blank">Clínica Cardiológica Cardiocom.</a>

           <a href="http://www.dhainformatica.com.br/" target="_blank"><img class="img-responsive" src="<?php echo $caminho;?>imgs/dha.png" alt="logo do site D.H.A. Informática"></a>
         <a href="http://www.dhainformatica.com.br/" target="_blank">Desenvolvimento de Sistemas Personalizados.</a>
        </div>
        </div>
</aside>
<!-- Término do sidebar -->
</div>



<!-- Incluindo o rodapé padrão no documento -->
<?php
	include $caminho."includes/footer.php";
?>
<!-- Término da inclusão do rodapé padrão no documento -->

<!-- Incluindo os scripts de JQuery e bootstrap.js -->
<?php
	include $caminho."includes/scripts.php";
 ?>
<!-- Término da inclusão do JQuery e bootstrap.min -->


 </body>
 </html>
