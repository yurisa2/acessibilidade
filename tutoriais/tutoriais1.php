<?php
session_start();

$caminho ="../";

$pagina = "Tutoriais";
?>

<?php session_start(); ?><!DOCTYPE html>
<html>

<!-- Incluindo o head padrão no documento -->
<?php
  include $caminho."includes/head.php";
 ?>
 <!-- Término da inclusão do head padrão no documento -->

 <body class="tutoriais">

<!-- Incluindo o navbar padrão no documento -->
<?php
    include $caminho."includes/nav.php";
 ?>
<!-- Término da inclusão do navbar padrão no documento -->

  <?php
    include "header_tutoriais.php";
  ?>

    <!--INICIO DO POST-->
    <div class="container">
        <div class="col-md-9">
    <!---->
        <div class="col-md-12">
         <article>
              <img src="<?php echo $caminho;?>imgs/imgs-tutoriais/linuxforbeginners-695x362.jpg" alt="Logotipo do Linux" title="Logotipo do Linux" class="img-responsive">
                <div class="col-md-12">
              <h2 class="color-tutoriais"><strong>05 distros Linux para usuários iniciantes</strong></h2><br>
              <p class="text-justify">Se você está curioso sobre o Linux que cada vez mais é notícia no mundo da tecnologia e quer entrar nessa onda, acompanhe abaixo uma lista com 05 distribuições fáceis para você começar.</p><br>

              <h2 class="color-tutoriais"><strong>01 - UBUNTU</strong></h2>
              <p class="text-justify">Ubuntu é um sistema operacional de código aberto, construído a partir do núcleo Linux, baseado no Debian. É patrocinado pela Canonical Ltd (dirigida por Jane Silber). O Ubuntu diferencia-se do Debian por ter versões lançadas semestralmente, por disponibilizar suporte técnico nos 9 meses seguintes ao lançamento de cada versão (as versões LTS – Long Term Support – para desktop recebem 5 anos de suporte, e para servidor recebem 5 anos de suporte), e pela filosofia em torno de sua concepção. A proposta do Ubuntu é oferecer um sistema que qualquer pessoa possa utilizar sem dificuldades, independentemente de nacionalidade, nível de conhecimento ou limitações físicas. O sistema deve ser constituído principalmente por software livre. Deve também ser isento de qualquer taxa.</p><br>
              <a href="<?php echo $caminho;?>imgs/imgs-tutoriais/ubuntu-695x464.jpg"><img src="<?php echo $caminho;?>imgs/imgs-tutoriais/ubuntu-695x464.jpg" class="img-responsive" alt="Imagem mostrando a tela do Sistema Operacional Ubuntu " title="UBUNTU"></a>

              <h2 class="color-tutoriais"><strong>02 - Pinguy OS</strong></h2>
              <p class="text-justify">O Pinguy OS é uma distribuição para Ubuntu. Há quem diga que é uma cópia da velha distro direcionada aos usuários Linux, as com facilidades que o Ubuntu não tem. Isso pode ser observado desde a interface até às funcionaliaddes do sistema. Mais simples e mais prático que o Ubuntu, o Pinguy OS traz opções implementadas, assim ao instalar o sistema, o usuário não precisa baixar aplicativos para começar a rodar o soft na máquina.</p><br>
              
              <a href="<?php echo $caminho;?>imgs/imgs-tutoriais/pinguyos.png"><img src="<?php echo $caminho;?>imgs/imgs-tutoriais/pinguyos.png" class="img-responsive" alt="Imagem mostrando a tela do Sistema Operacional PinguyOS" title="PinguyOS"></a>

              <h2 class="color-tutoriais"><strong>03 - Linux Mint</strong></h2>
              <p class="text-justify">Linux Mint é um sistema operacional baseado nas distribuições Ubuntu e Debian, que fornece versões com os principais desktops atuais (Cinnamon, MATE, KDE e Xfce). Essa distribuição oferece um desktop elegante e confortável, além de poderoso e fácil de usar</p><br>
              
              <a href="<?php echo $caminho;?>imgs/imgs-tutoriais/Linux%20Mint.png"><img src="<?php echo $caminho;?>imgs/imgs-tutoriais/Linux%20Mint.png" class="img-responsive" alt="Imagem mostrando a tela do Sistema Operacional Linux Mint " title="Linux Mint"></a>
              
              <h2 class="color-tutoriais"><strong>04 - Linuxfx</strong></h2>

              <p class="text-justify"> Desenvolvido sobre uma estrutura sólida, esta nova versão do Linuxfx é a última da série 7 e traz várias novidades embarcadas. Comece utilizando a nova interface ctOS e todos os recursos dos sistemas desenvolvidos pela Linuxfx Software para a área de biometria e controle de acesso. Esta versão contém as mais atuais ferramentas para produção, ou até mesmo navegação na internet, além de fornecer segurança extra contra sites maliciosos ou outros conteúdos potencialmente prejudiciais ao seu computador. </p><br>
              <a href="<?php echo $caminho;?>imgs/imgs-tutoriais/Linuxfx.jpg"><img src="<?php echo $caminho;?>imgs/imgs-tutoriais/Linuxfx.jpg" class="img-responsive" alt="Imagem mostrando a tela do Sistema Operacional Linuxfx " title="Linuxfx"></a>
              
              <h2 class="color-tutoriais"><strong>05 - Linux Deepin</strong></h2>
              <p class="text-justify">Deepin é uma distribuição Linux baseada no Debian Sid, que fornece um sistema operacional elegante, amigável e estável. Deepin concentra-se na experiência user-friendly e design bonito, por isso é fácil de instalar e usar para a maioria dos usuários e pode muito bem substituir o sistema Windows para trabalho e entretenimento.</p><br>
              <a href="<?php echo $caminho;?>imgs/imgs-tutoriais/Linux%20Deepin.png"><img src="<?php echo $caminho;?>imgs/imgs-tutoriais/Linux%20Deepin.png" class="img-responsive" alt="Imagem mostrando a tela do Sistema Operacional Linux Deepin " title="Linux Deepin"></a><br>

              </div>
          </article>
         <!--INICIO QUEM EU SOU-->
          <?php
            include "include_quem_sou.php";
          ?>
          <!--fIM QUEM EU SOU-->
        </div>
    </div>

    <?php
      include "include_sidebar.php";
    ?>

    </div> <!-- /container -->

<!-- Incluindo o rodapé padrão no documento -->
<?php
  include $caminho."includes/footer.php";
?>
<!-- Término da inclusão do rodapé padrão no documento -->

<!-- Incluindo os scripts de JQuery e bootstrap.js -->
<?php
  include $caminho."includes/scripts.php";
 ?>
<!-- Término da inclusão do JQuery e bootstrap.min -->
  </body>
</html>
