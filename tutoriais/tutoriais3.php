<?php
session_start();

$caminho ="../";

$pagina = "Tutoriais";
?>

<?php session_start(); ?><!DOCTYPE html>
<html>
<!-- Incluindo o head padrão no documento -->
<?php
  include $caminho."includes/head.php";
 ?>
 <!-- Término da inclusão do head padrão no documento -->

 <body class="tutoriais">

<!-- Incluindo o navbar padrão no documento -->
<?php
    include $caminho."includes/nav.php";
 ?>
<!-- Término da inclusão do navbar padrão no documento -->
    <?php
      include "header_tutoriais.php";
    ?>

    <!--INICIO DO POST-->
    <div class="container">
        <div class="col-md-9">
    <!---->
        <div class="col-md-12">
         <article>

            <div class="col-md-12">

              <h2 class="color-tutoriais"><strong>Como instalar o RedHat</strong></h2><br>
              <img src="<?php echo $caminho;?>imgs/imgs-tutoriais/redhat.jpg" class="img-responsive" alt="Imagem mostrando o Logotipo do Sistema Operacional RedHat e parte do seu funcionamento" title="RedHat">

              <h2 class="color-tutoriais"><strong>1º PASSO</strong></h2>
              <p class="text-justify">Como sempre, o primeiro passo é dar boot através do CD-ROM de instalação. Caso você esteja usando um PC antigo, que ainda não suporte este recurso, basta fazer os disquetes de boot. A partir da versão 8.1 (a versão final deve estar disponível em Fevereiro de 2003) o layout dos disquetes de boot mudou um pouco. Para instalar a partir do CD ou a partir de arquivos copiados para uma partição do HD é preciso fazer um único disquete, gravando a imagem bootdisk.img, encontrada na pasta /images do CD1. Para instalar via rede é necessário um segundo disquete, o drvnet.img (placas PCI ou ISA) ou o pcmciadd.img (para notebooks com placas PCMCIA). Os disquetes são necessários nos casos em que o PC não suporta boot via CD-ROM ou caso você esteja usando um drive USB. A partir do Red Hat 8.0 o instalador se oferece para testar a integridade dos CDs de instalação através do md5sum. Este é um teste bit a bit que garante que não existe nenhum problema de gravação. Se um único bit vier alterado você é avisado do problema. Num CD-ROM de 48x o teste demora em média 4 minutos para cada CD, vale à pena. A primeira pergunta do instalador sobre a linguagem se aplica apenas à linguagem que será usada durante a instalação. A escolha da linguagem do sistema é feita bem mais pra frente, quase no final da instalação onde você pode instalar "linguagens adicionais". O Português do Brasil está entre as opções nas duas etapas, uma vez instalado o suporte os programas já são configurados automaticamente. Na hora de particionar o disco você terá a opção de usar o fdisk (em modo texto, recomendável apenas se você já sabe trabalhar com ele) e o Disk Druid, que possui uma interface gráfica bastante amigável. Evite usar o particionamento automático, pois ele presume que você quer instalar o Red Hat como sistema operacional principal e apaga todos os dados do HD.</p><br>
              
              <a href="<?php echo $caminho;?>imgs/imgs-tutoriais/redhat-install01.jpg"><img src="<?php echo $caminho;?>imgs/imgs-tutoriais/redhat-install01.jpg" class="img-responsive" alt="Imagem mostrando a janela de instalação do Sistema Operacional RedHat" title="1º PASSO"></a>

              <h2 class="color-tutoriais"><strong>2º PASSO</strong></h2>
              <p class="text-justify">No Disk Druid basta clicar sobre as partições, representadas pelo mapa na parte superior da tela e acessar as opções para deletar, criar nova partição e assim por diante. As regras são as mesmas válidas para o Mandrake e Slackware, ou seja, criar uma partição raiz de uns 4 a 5 GB, uma partição /home englobando a maior parte do disco, uma partição swap e, opcionalmente, outras partições desejadas.</p><br>
              <a href="<?php echo $caminho;?>imgs/imgs-tutoriais/redhat-install02.jpg"><img src="<?php echo $caminho;?>imgs/imgs-tutoriais/redhat-install02.jpg" class="img-responsive" alt="Imagem mostrando a janela de instalação do Sistema Operacional RedHat" title="2º PASSO"></a>

              <h2 class="color-tutoriais"><strong>3º PASSO</strong></h2>
              <p class="text-justify">O Red Hat possui algumas limitações no particionamento do disco. Ele não oferece a opção de redimensionar partições Windows, nem de formatar partições em ReiserFS (apenas EXT3 e outros sistemas). Ao contrário de outras distribuições ele não bipassa a limitação de placas mãe antigas quanto a HDs maiores de 8 GB (o problema dos 1024 cilindros). Caso você esteja usando um PC antigo que possua esta limitação você terá que criar a partição raiz (/) do sistema dentro dos primeiros 8 GB e criar outras partições (/home por exemplo) englobando o restante. Como disse, outras distribuições são capazes de bipassar esta limitação do BIOS, dando boot a partir de qualquer lugar do disco. Se você for instalar o Red Hat junto com outras distribuições prefira deixar o Red Hat numa partição logo no início do disco. Na hora de instalar o gerenciador de boot, existe a opção de instalar o Grub ou o Lilo. O Lilo não é oficialmente suportado pelo Red Hat e o pacote inclui uma versão antiga, aparentemente incluída apenas por formalidade. É recomendável usar mesmo o Grub, que vem configurado com um menu gráfico bastante agradável. Em algumas instalações o Lilo incluído não é sequer capaz de inicializar o sistema depois da instalação sem ajustes manuais. O gerenciador de boot é sempre instalado na MBR por default, caso você queira instala-lo na partição (dual-boot com outras distribuições Linux ou BSD) acesse as opções avançadas.</p><br>
              
              <a href="<?php echo $caminho;?>imgs/imgs-tutoriais/redhat-install03.jpg"><img src="<?php echo $caminho;?>imgs/imgs-tutoriais/redhat-install03.jpg" class="img-responsive" alt="Imagem mostrando a janela de instalação do Sistema Operacional RedHat" title="3º PASSO"></a>
              <h2 class="color-tutoriais"><strong>4º PASSO</strong></h2>

              <p class="text-justify">Na hora de configurar a rede, o próprio instalador se encarrega de detectar as placas de rede e hardmodems instalados no sistema e pedir as configurações de cada um. Lembre-se que o instalador não é capaz de detectar softmodems, que devem ser instalados depois, seguindo as instruções do capítulo 4 deste livro.</p><br>
              <a href="<?php echo $caminho;?>imgs/imgs-tutoriais/redhat-install04.png"><img src="<?php echo $caminho;?>imgs/imgs-tutoriais/redhat-install04.png" class="img-responsive" alt="Imagem mostrando a janela de instalação do Sistema Operacional RedHat" title="4º PASSO"></a>
              <h2 class="color-tutoriais"><strong>5º PASSO</strong></h2>

              <p class="text-justify">O Red Hat inclui um firewall simplificado que permite bloquear o acesso a alguns serviços instalados no sistema, como o Apache, FTP, SSH, etc. Você deve indicar qual é a interface de rede "confiável" ou seja, a placa que está ligada aos demais micros da rede local e qual é a interface ligada à internet. O objetivo do firewall é permitir que os clientes da rede local possam acessar os serviços normalmente, ao mesmo tempo em que os acessos vindos da Internet são barrados até que dito o contrário.</p><br>
              <a href="<?php echo $caminho;?>imgs/imgs-tutoriais/redhat-install05.jpg"><img src="<?php echo $caminho;?>imgs/imgs-tutoriais/redhat-install05.jpg" class="img-responsive" alt="Imagem mostrando a janela de instalação do Sistema Operacional RedHat" title="5º PASSO"></a><br>
              <h2 class="color-tutoriais"><strong>6º PASSO</strong></h2>

              <p class="text-justify">Em seguida basta configurar a senha de root e criar os logins dos usuários do sistema. Lembre-se de manter ativadas as opções "Enable MD5 Passwords" e "Enable Shadow Passwords" que habilitam respectivamente o suporte a senhas de mais de 8 caracteres e o armazenamento de senhas em forma encriptada. Bem, senhas com menos de 8 caracteres e ainda por cima gravadas no disco em texto puro não são muito recomendáveis não é mesmo? :-)</p><br>
              <p>Depois disso chegamos ao ponto alto da instalação, que é a seleção dos pacotes. Os pacotes estão divididos em poucas categorias, as tradicionais "Gnome", "KDE", "Jogos", "Escritório", "Desenvolvimento", etc. É recomendável marcar sempre tanto o Gnome quanto o KDE, além da categoria de desenvolvimento caso você pretenda instalar programas a partir do código fonte. Dentro de cada categoria você tem acesso a alguns os programas incluídos nela:</p><br>
              
              <a href="<?php echo $caminho;?>imgs/imgs-tutoriais/redhat-install06.jpg"><img src="<?php echo $caminho;?>imgs/imgs-tutoriais/redhat-install06.jpg" class="img-responsive" alt="Imagem mostrando a janela de instalação do Sistema Operacional RedHat" title="6º PASSO"></a><br>
              <p class="text-justify">O Red Hat é grande, é recomendável reservar uma partição de pelo menos 4 GB. Uma instalação típica do 8.0 consome pouco mais de 2 GB, enquanto uma completa consome quase 3 GB. A maior parte dos pacotes são obrigatórios, por isso não é possível fazer uma instalação mínima. Mesmo desmarcando todas as opções do menu a instalação ainda consumirá 1.3 GB. Se você pretende instalar em menos que isso, a alternativa é usar o Slackware ou Mandrake. Outra ressalva é que o Red Hat (assim como o Mandrake) é recomendável apenas para micros Pentium II ou K6-2 em diante com pelo menos 128 MB. É possível instalar o sistema em micros com apenas 64 MB mas o desempenho fica comprometido. Para micros antigos o ideal é utilizar o Slackware ou o Vector Linux. Veja detalhes sobre como instalar o Linux em micros antigos no capítulo 6 deste livro. A cópia dos pacotes demora um pouco devido à grande quantidade de softwares, mas o restante da instalação é bastante tranqüila. Para o final fica faltando apenas a configuração do vídeo, que se resume basicamente a indicar qual é a resolução desejada, já que o instalador detecta a placa de vídeo e o monitor usados.</p><br>

              </div>
          </article>

         <!--INICIO QUEM EU SOU-->
         <?php
            include "include_quem_sou.php";
          ?>
          <!--fIM QUEM EU SOU-->
        </div>
    </div>
    <?php
      include "include_sidebar.php";
    ?>

    </div> <!-- /container -->

<!-- Incluindo o rodapé padrão no documento -->
<?php
  include $caminho."includes/footer.php";
?>
<!-- Término da inclusão do rodapé padrão no documento -->

<!-- Incluindo os scripts de JQuery e bootstrap.js -->
<?php
  include $caminho."includes/scripts.php";
 ?>
<!-- Término da inclusão do JQuery e bootstrap.min -->
  </body>
</html>
