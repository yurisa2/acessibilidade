<?php
session_start();

$caminho ="../";

$pagina = "Tutoriais";
?>

<?php session_start(); ?><!DOCTYPE html>
<html>

<!-- Incluindo o head padrão no documento -->
<?php
  include $caminho."includes/head.php";
 ?>
 <!-- Término da inclusão do head padrão no documento -->

 <body class="tutoriais">

<!-- Incluindo o navbar padrão no documento -->
<?php
    include $caminho."includes/nav.php";
 ?>
<!-- Término da inclusão do navbar padrão no documento -->
    <?php
      include "header_tutoriais.php";
    ?>
   <!--INICIO DO POST-->
    <div class="container">
        <div class="col-md-9">
        <div class="col-md-12">
         <article>
            <img src="<?php echo $caminho;?>imgs/imgs-tutoriais/canonical-ubuntu-logo-1.jpg" class="img-responsive" alt="Logotipo do Ubuntu" title="Logotipo do Ubuntu">
            <div class="col-md-12">
              <h2  class="color-tutoriais"><strong>Como instalar o Ubuntu 14.04 corretamente</strong></h2><br>
              <p class="text-justify">Muitos novatos no Linux ainda tem algumas dúvidas quanto a instalação e por vezes acabam adianto a ótimo oportunidade de testar o sistema por não saber instalar, em cá entre nós, muitas vezes até usuários antigos tem algumas dúvidas. Pensando nisso, resolvemos criar um tutorial bem completo abordando todos os detalhes cruciais da instalação do Ubuntu no seu computador, seja você a pessoa que quer fazer um dual boot (usar o Windows e o Linux na mesma máquina), seja você a pessoa que quer eliminar o sistema da Microsoft do HD e usar apenas Linux. Confira o vídeo, ele é extenso pois tem muitos detalhes, então prepara a sua pipoca aí e aprecie sem moderação, aposto que ao final dele você terá muito mais conhecimento e se sentirá mais seguro para usar o Ubuntu.</p><br>
            <div class="embed-responsive embed-responsive-16by9">
              <iframe class="embed-responsive-item" width="765" height="464" src="https://www.youtube.com/embed/ShH2U4D5tjM" frameborder="0" allowfullscreen></iframe>
            </div>
            <div class="col-md-12">
                      <p class="text-right"><em>Video do Canal <mark>RBtech</mark></em></p>
                  </div>
             </div>
          </article>
    <!--FIM DO POST-->
    <!--INICIO QUEM EU SOU-->
         <?php
            include "include_quem_sou.php";
          ?>
          <!--fIM QUEM EU SOU-->
        </div>
    </div>
    <!--INICIO DO SIDEBAR-->
    <?php
      include "include_sidebar.php";
    ?>
    <!--FIM DO SIDEBAR-->

    </div> <!-- /container -->

<!-- Incluindo o rodapé padrão no documento -->
<?php
  include $caminho."includes/footer.php";
?>
<!-- Término da inclusão do rodapé padrão no documento -->

<!-- Incluindo os scripts de JQuery e bootstrap.js -->
<?php
  include $caminho."includes/scripts.php";
 ?>
<!-- Término da inclusão do JQuery e bootstrap.min -->
  </body>
</html>
