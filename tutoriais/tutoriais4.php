<?php
session_start();

$caminho ="../";

$pagina = "Tutoriais";
?>

 <?php session_start(); ?><!DOCTYPE html>
<html>

<!-- Incluindo o head padrão no documento -->
<?php
  include $caminho."includes/head.php";
 ?>
 <!-- Término da inclusão do head padrão no documento -->

 <body class="tutoriais">

<!-- Incluindo o navbar padrão no documento -->
<?php
  include $caminho."includes/nav.php";
 ?>
<!-- Término da inclusão do navbar padrão no documento -->
    <?php
      include "header_tutoriais.php";
    ?>
   <!--INICIO DO POST-->
    <div class="container">
        <div class="col-md-9">
        <div class="col-md-12">
         <article>
            <div class="col-md-12 well well-lg">
                <!--<h2 class="color-tutoriais" style="margin-left: 5px;"><strong>Downloads</strong></h2>-->
                <h3  class="color-tutoriais" style="margin-left: 5px;"><i class="fa fa-lightbulb-o" aria-hidden="true"></i><strong>DICAS</strong></h3>
            </div>
            <div class="col-md-12 media">
              <div class="media-left media-middle">
                <a href="<?php echo $caminho;?>imgs/imgs-tutoriais/debianfordummiesbook.jpg">
                  <img class=" livrodummies media-object" src="<?php echo $caminho;?>imgs/imgs-tutoriais/debianfordummiesbookthumb.jpg" alt="Imagem mostrando a capa do livro Debian for Dummies" title=" Livro Debian for Dummies">
                </a>
              </div>
              <div class="media-body">
                <h4 class="media-heading color-tutoriais">Debian for Dummies</h4>
                <p>Inclui Debian GNU / Linux em 2 CD-ROMs! Obtenha ajuda na instalação e configuração do Debian GNU / Linux!</p><br>
                <p>Seu guia para um sistema seguro, estável Debian Este guia amigável irá ajudá-lo a juntar-se à crescente comunidade de usuários do sistema operacional de código aberto. Com estas dicas quentes, você vai ter o sistema instalado e funcionando em nenhum momento! tudo isso em 2 de bônus CD-ROMs Debian GNU / Linux versão de distribuição de demonstração da suíte de escritório Applixware para a versão de Avaliação Linux da Tripwire 2.0 para Linux DiskCheck 3.1.1 utilitário para monitorar o espaço em disco rígido e mais... </p>
                <p><a  class="btn btn-default btn-lg color-tutoriais" href="http://www.amazon.com/s/183-2192334-2153748?ie=UTF8&index=books&keywords=0764507133&link_code=qs&tag=bookcrossingc-20" role="button"><span class="glyphicon glyphicon-usd" aria-hidden="true"></span>Comprar</a></p>
              </div>
              <h2 class="color-tutoriais"><strong>Debian</strong></h2>
              <p class="text-justify">O Projeto Debian é uma associação de indivíduos que têm como causa comum criar um sistema operacional livre. O sistema operacional que criamos é chamado Debian. Um sistema operacional é o conjunto de programas básicos e utilitários que fazem seu computador funcionar. No núcleo do sistema operacional está o kernel. O kernel é o programa mais fundamental no computador e faz todas as operações mais básicas, permitindo que você execute outros programas. Os sistemas Debian atualmente usam o kernel Linux ou o kernel FreeBSD. O Linux é uma peça de software criada inicialmente por Linus Torvalds com a ajuda de milhares de programadores espalhados por todo o mundo. O FreeBSD é um sistema operacional incluíndo um kernel e outros softwares. No entanto, há trabalho em andamento para fornecer o Debian com outros kernels, principalmente com o Hurd. O Hurd é um conjunto de servidores que rodam no topo de um micro kernel (como o Mach), os quais implementam diferentes características. O Hurd é software livre produzido pelo projeto GNU. Uma grande parte das ferramentas básicas que formam o sistema operacional são originadas do projeto GNU; daí os nomes: GNU/Linux, GNU/kFreeBSD e GNU/Hurd. Essas ferramentas também são livres.</p><br>
              
              <a href="<?php echo $caminho;?>imgs/imgs-tutoriais/debian-logo.jpg"><img src="<?php echo $caminho;?>imgs/imgs-tutoriais/debian-logo.jpg" class="img-responsive" alt="Imagem mostrando a tela do Sistema Operacional Debian " title="Debian"></a>
              <p class="text-justify">Claro que o que todos queremos são aplicativos: programas que nos ajudam a conseguir fazer o que desejamos fazer, desde edição de documentos até a administração de negócios, passando por jogos e desenvolvimento de mais software. O Debian vem com mais de 43000 pacotes (softwares pré-compilados e empacotados em um formato amigável, o que faz com que sejam de fácil instalação em sua máquina), um gerenciador de pacotes (APT) e outros utilitários que tornam possível gerenciar milhares de pacotes em milhares de computadores de maneira tão fácil quanto instalar um único aplicativo. Todos eles são livres. É mais ou menos como uma torre: Na base dela está o kernel. Sobre ele todas as ferramentas básicas e acima estão todos os outros softwares que você executa em seu computador. No topo da torre está o Debian — organizando e arrumando cuidadosamente as coisas, de modo que tudo funcione bem enquanto todos esses componentes trabalham em conjunto.</p><br>
            </div>

         </article>
    <!--FIM DO POST-->
    <!--INICIO QUEM EU SOU-->
          <?php
            include "include_quem_sou.php";
          ?>
    <!--FIM QUEM EU SOU-->
         </div>
    </div>
    <!--INICIO DO SIDEBAR-->
    <?php
      include "include_sidebar.php";
    ?>
    <!--FIM DO SIDEBAR-->

    </div> <!-- /container -->

<!-- Incluindo o rodapé padrão no documento -->
<?php
  include $caminho."includes/footer.php";
?>
<!-- Término da inclusão do rodapé padrão no documento -->

<!-- Incluindo os scripts de JQuery e bootstrap.js -->
<?php
  include $caminho."includes/scripts.php";
 ?>
<!-- Término da inclusão do JQuery e bootstrap.min -->
  </body>
</html>
