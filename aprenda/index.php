<?php
session_start();

$caminho ="../";
?>

<?php session_start(); ?><!DOCTYPE html>
<html>
<?php
	$pagina = "Aprenda";
?>
<!-- Incluindo o head padrão no documento -->
<?php
	include $caminho."includes/head.php";
 ?>
 <!-- Término da inclusão do head padrão no documento -->

 <body>

<!-- Incluindo o navbar padrão no documento -->
<?php
    include $caminho."includes/nav.php";
 ?>
<!-- Término da inclusão do navbar padrão no documento -->

<?php
    include $caminho."aprenda/aprenda-header.php";
 ?>
<!-- Inicio do conteúdo -->
<div class="container">
<div class="col-lg-10 col-md-10 aprenda">
	<div class="post-principal row">
	    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
			<img class="center-block img-responsive" src="<?php echo $caminho;?>imgs/imgs-aprenda/aprenda1.png" title="logo da sessão um" alt="imagem da tela do software Furious Iso Mount">
		</div>
		<div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
	        <a class="link-post" href="">Aprenda Como Criar Uma Imagem ISO no <?php echo dah_distro(); ?></a>
	        <p>Como as mídias de DVD/CD estão obsoletas a cada dia, ter um software que possa montar nossas imagens ISO é imprescindível. Existem alguns softwares para Linux que cumprem o que prometem, mas eu particularmente prefiro fazer via linha de comando (é mais prático ?? Portanto, se você deseja montar suas imagens ISO facilmente no Linux, eu recomendo que leia esse artigo!</p>
		</div>
	</div>

	<div class="post2 row">
		<div class="col-lg-3 col-md-3">
				<img class="center-block img-responsive" src="<?php echo $caminho;?>imgs/imgs-aprenda/aprenda2.png" title="logo da sessão dois" alt="imagem da tela do software Cuttlefish">
		</div>
		<div class="col-lg-9 col-md-9">
	            <a class="link-post" href="">Aprenda a Aumentar Sua Produtividade...</a>
	            <p>Quando iniciamos o Sistema Operacional, diversos serviços são carregados simultaneamente. E logo após isso, podemos iniciar nossas tarefas corriqueiras… como: abrir um editor de texto, um navegador web, etc… Por isso, para aumentar sua produtividade, é recomendável que automatize suas tarefas levando em conta que; cada evento a ser realizado possa disparar a execução de alguma tarefa!! Por exemplo, quando não estivesse escutando nenhuma música no seu player, o volume estaria no mínimo ou desativado; mas quando executasse alguma playlist favorita o volume do áudio seria ajustado automaticamente!</p>
		</div>
	</div>

	<div class="post2 row">
		<div class="col-lg-9 col-md-9">
	            <a class="link-post" href="">Aprenda A Procurar Softwares Maliciosos</a>
	            <p>“Nenhum sistema é 100% seguro”… e os sistemas Linux não poderiam ficar fora disso. Existem diversas maneiras para se comprometer um sistema Linux; em ambientes de servidores Linux é bastante comum a busca por vulnerabilidades deixadas no sistema utilizando recursos conhecidos como exploit. Sendo assim, temos como principais ameaças existentes, para um sistema Linux, o que denomina-se “rootkit”. Com ele é possível adquirir o acesso “root” do sistema e obter controle total sobre ele. Portanto, para que se tenha noções reais sobre a integridade do nosso sistema, devemos utilizar ferramentas responsáveis em vasculhar a presença desses softwares maliciosos</p>
		</div>
        <div class="col-lg-3 col-md-3">
				<img class="center-block img-responsive" src="<?php echo $caminho;?>imgs/imgs-aprenda/aprenda3.png" title="logo da sessão três" alt="imagem de um dedo sobre uma tela de computador exibindo as linhas de comando">

		</div>
	</div>

	<div class="post3 row">
        <div class="col-lg-3 col-md-3">
				<img class="center-block img-responsive" src="<?php echo $caminho;?>imgs/imgs-aprenda/aprenda4.png" title="logo da sessão quatro" alt="imagem de um pinguim sobreposto a tela cópia da tela do software Timeshift">
		</div>
		<div class="col-lg-9 col-md-9">
	            <a class="link-post" href="">Aprenda A Restaurar Seu Sistema...</a>
	            <p>Quem nunca quis dá um CTRL+Z no momento em que fez ou aconteceu algo errado? Aquele botão “desfazer”, encontrado em diversos programas, resolveria muitas coisas caso tivesse disponível para restaurar seu sistema Linux em situações inesperadas. Sendo assim, conheça algumas maneiras para restaurar seu sistema Linux em caso de falhas, possivelmente reversíveis é claro</p>
		</div>
    </div>

    <div class="post2 row">
		<div class="col-lg-9 col-md-9">
	            <a class="link-post" href="">Aprenda A Acessar uma máquina Ubuntu...</a>
	            <p>Depois de iniciado no universo Linux, provavelmente você deseja ter acesso as ferramentas mais avançadas que possam te oferecer recursos previstos em outros sistemas. Nesse tópico, um recurso interessante seria o acesso a outra máquina Linux, no caso com o Ubuntu, em conexão remota; a fim de controlar outra máquina remotamente. Sendo assim, será mostrado o uso do terminal de comandos e o uso do recurso gráfico; através das ferramentas SSH e o programa Remmina, respectivamente.</p>
		</div>
        <div class="col-lg-3 col-md-3">
				<img class="center-block img-responsive" src="<?php echo $caminho;?>imgs/imgs-aprenda/aprenda5.png" title="logo da sessão cinco" alt="imagem da tela de log de acesso remoto do linux">
		</div>
	</div>

</div>

<!-- Incluindo o aside -->
<?php
    include $caminho."aprenda/barside.php";
?>
<!-- Términdo do aside -->
<!-- Término do conteúdo -->
</div>



<!-- Incluindo o rodapé padrão no documento -->
<?php
	include $caminho."includes/footer.php";
?>
<!-- Término da inclusão do rodapé padrão no documento -->

<!-- Incluindo os scripts de JQuery e bootstrap.js -->
<?php
	include $caminho."includes/scripts.php";
 ?>
<!-- Término da inclusão do JQuery e bootstrap.min -->

 </body>
</html>
