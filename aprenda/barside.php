

<!-- Inicio do sidebar -->
<aside>
        <div class="col-xs-3 col-md-2">
        <div class="hidden-xs hidden-sm">
            <div class="sidebar-aprenda">
               <a href="http://www.congeladosdamayra.com.br" target="_blank"><img class="img-responsive" src="<?php echo $caminho;?>imgs/mayra.png" title="logo do site Congelados da Mayra" alt="desenho de uma mulher com touca de cozinheira"></a>
    	       <p>Congelados da Mayra, É só descongelar e apreciar.</p>
            </div>

            <div class="sidebar-aprenda">
               <a href="http://sansupersonicprod.com/" target="_blank"><img class="img-responsive" src="<?php echo $caminho;?>imgs/san.png" title="logo do site Sans Super Sonic Prod." alt="desenho de uma caveira estilizada"></a>
    	       <p>Artes Gráficas, Padronização e Vídeo.</p>
            </div>

            <div class="sidebar-aprenda">
               <a href="http://www.arvpaisagismo.eco.br/" target="_blank"><img class="img-responsive" src="<?php echo $caminho;?>imgs/arv.png" title="logo do site ARV Paisagismo" alt="sigla ARV estilizada com o desenho de algumas folhas"></a>
    	       <p>Jardins, Paisagismo, Plantio de Grama, etc.</p>
            </div>

            <div class="sidebar-aprenda">
               <a href="http://www.vickysperfumes.com.br/" target="_blank"><img class="img-responsive" src="<?php echo $caminho;?>imgs/vickys.png" title="logo do site Vicky's Perfumes" alt="palavra Vicys estilizada"></a>
    	       <p>Perfumes Importados e Acessórios.</p>
            </div>

            <div class="sidebar-aprenda">
               <a href="http://www.martinssociedadeadvogados.com.br/" target="_blank"><img class="img-responsive" src="<?php echo $caminho;?>imgs/martins.png" title="logo do site Martins Sociedade Advogados" alt="Desenho estilizado com a cor dourada"></a>
    	       <p>Direito da Família, Direito Civil, Direito do Trabalho, Direito das Sucessões.</p>
            </div>

            <div class="sidebar-aprenda">
               <a href="http://cardiocom.com.br/" target="_blank"><img class="img-responsive" src="<?php echo $caminho;?>imgs/cardiocom.png" title="logo do site Cardiocom" alt="desenho de um coração vermelho com alguns traços em preto"></a>
    	       <p>Clínica Cardiológica Cardiocom.</p>
            </div>

            <div class="sidebar-aprenda">
               <a href="http://www.dhainformatica.com.br/" target="_blank"><img class="img-responsive" src="<?php echo $caminho;?>imgs/dha.png" title="logo do site D.H.A. Informática" alt="sigla D H A distribuida em um desenho azul em degradê"></a>
    	       <p>Desenvolvimento de Sistemas Personalizados.</p>
            </div>
        </div>
        </div>
</aside>
<!-- Término do sidebar -->
