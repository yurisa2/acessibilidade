<?php
session_start();

include 'include/include.php';
include 'include/content.php';


$caminho ="../";
?>

<?php session_start(); ?><!DOCTYPE html>
<html>
<?php
	$pagina = "Distros";
?>
<!-- Incluindo o head padrão no documento -->
<?php
	include $caminho."includes/head.php";
 ?>
 <!-- Término da inclusão do head padrão no documento -->

 <body>

<!-- Incluindo o navbar padrão no documento -->
<?php
    include $caminho."includes/nav.php";
 ?>
<!-- Término da inclusão do navbar padrão no documento -->

<!-- Inicio do conteúdo -->

<?php echo render_page($_POST["page"]); ?>

<!-- Término do conteúdo -->



<!-- Incluindo o rodapé padrão no documento -->
<?php
	include $caminho."includes/footer.php";
?>
<!-- Término da inclusão do rodapé padrão no documento -->

<!-- Incluindo os scripts de JQuery e bootstrap.js -->
<?php
	include $caminho."includes/scripts.php";
 ?>
<!-- Término da inclusão do JQuery e bootstrap.min -->

 </body>
