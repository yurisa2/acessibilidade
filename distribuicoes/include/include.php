<?php
function dah_link_distro($distro = '',$button = true,$form_id = true)
{

    if($form_id == true) $formulario_id = ' id="#'.urlencode($distro).'"'; //Mais uma gambiarra para validar

    if($distro == '') $distro = dah_distro();
    //$retorno_conteudo .= '<a href="?page=distro_individual&distro='.urlencode($distro).'" class="btn btn-info btn-sm btn-dah-distro">'.$distro.'</a>';

    $retorno_conteudo .= '<form method="post" action="#"'.$formulario_id.'>
    <input type="hidden" name="page" value="distro_individual" class="form">
    <input type="hidden" name="distro" value="'.$distro.'">';

    if($button == true) $retorno_conteudo .= '<button type="submit" name="page" value="distro_individual" class="btn btn-info btn-sm btn-dah-distro">'.$distro.'</button>'; // PQ senão não valida né´tio.
    else {
      $retorno_conteudo .= $distro;
    }

    $retorno_conteudo .= '</form>
    ';
   return $retorno_conteudo;

}

function dah_link_generico($page,$label,$distro,$button = true)
{
    $distro=dah_distro();

    $retorno_conteudo .= '
    <form method="post" action="#" id="#generico">
    <input type="hidden" name="page" value="'.$page.'">
    <input type="hidden" name="distro" value="'.$distro.'">';

    if($button == true) $retorno_conteudo .= '<button type="submit" class="btn btn-link">'.$label.'</button>'; // PQ senão não valida né´tio.
    else {
      $retorno_conteudo .= $label;
    }

    $retorno_conteudo .= '</form>';


   return $retorno_conteudo;

}

function dah_link_servers($page,$label,$distros_server_count,$button = true,$form_id = true)
{
    $distro=dah_distro();

    if($form_id==true)  $form_id = ' id="#servers"';  //Tudo pela validação!

    $retorno_conteudo .= '<form method="post" action="#"'.$form_id.'>
    <input type="hidden" name="page" value="'.$page.'">
    <input type="hidden" name="number_distros" value="'.$distros_server_count.'">';


    if($button == true) $retorno_conteudo .=     '<button type="submit" class="btn btn-link">'.$label.'</button>'; // PQ senão não valida né´tio.
    else {
      $retorno_conteudo .= $label;
    }

    $retorno_conteudo .= '</form>';


   return $retorno_conteudo;

}

function dah_link_loja($page,$label,$distros_loja_count,$button = true,$form_id = true)
{
    $distro=dah_distro();

    //$retorno_conteudo .= '<a href="?page=distro_individual&distro='.urlencode($distro).'" class="btn btn-info btn-sm btn-dah-distro">'.$distro.'</a>';

    if($form_id==true)     $form_id = ' id="#loja"';  //Tudo pela validação!

    $retorno_conteudo .= '<form method="post" action="#"'.$form_id.'>
    <input type="hidden" name="page" value="'.$page.'">
    <input type="hidden" name="number_distros_loja" value="'.$distros_loja_count.'">';


    if($button == true) $retorno_conteudo .=  '<button type="submit" class="btn btn-link">'.$label.'</button>'; // PQ senão não valida né´tio.
    else {
      $retorno_conteudo .= $label;
    }

    $retorno_conteudo .= '</form>';



   return $retorno_conteudo;

}

function dah_context()
{

	$table_contexts = array("danger","warning","success","active","");

	return $table_contexts[rand(0,4)];

}



function dah_table_header($col,$aside)
{
                                        if($col > 0) $dth .= '<th>Distro</th>';
                                        if($col > 1) $dth .= '<th>Usuarios</th>';
                                        if($col > 2) $dth .= '<th>% Pop.</th>';
                                        if($col > 3) $dth .= '<th>Tamanho</th>';


		if($aside == 1) $retorno_tbl_header.= '<div class="hidden-sm hidden-xs col-md-4 col-lg-4">';
		if($aside == 0) $retorno_tbl_header.= '<div class="center-block hidden-sm hidden-xs col-md-12 col-lg-12">';

        $retorno_tbl_header.=  '
								<div class="panel panel-default">
									<div class="panel-heading">
										<h3 class="panel-title center-block"><i class="fa fa-desktop fa-fw"></i> Utilização das Distros</h3>
									</div>
									<div class="panel-body">
										<div class="table-responsive">
    									<table class="table table-bordered table-hover table-striped">
    										<thead>
    											<tr>
    												'.$dth.'
    											</tr>
    										</thead>
    										<tbody>';

		return $retorno_tbl_header;

}



function dah_table_items($qty,$col)
{
		function dah_trs($col_inside)    // ALBERTO, PAGA UM PAU PARA A SUB FUNCTION
		{
			$trs = "";
			if($col_inside > 0) $trs .= '<td>'.dah_link_distro('',true,false).'</td>';
			if($col_inside > 1) $trs .= '<td>'.rand(100,10000).'</td>';
			if($col_inside > 2) $trs .= '<td>'.rand(0,100).'%</td>';
			if($col_inside > 3) $trs .= '<td>'.rand(100,2000).'MB</td>';

			return $trs;
		}

    for ($i = 1; $i <= $qty; $i++)
		{
        $table_item =  '<tr class="'.dah_context().'">'.dah_trs($col).'</tr></tbody>';  // USANDO A SUB_FUNC AQUI! MOFO!
     	$table_items_to_return .= $table_item;
         }

	return $table_items_to_return;

}


function dah_table_footer($aside)
{

	$retorno_footer = '  </table></div></div>
                    ';
	$retorno_footer .= '
  <div class="legenda subiu"></div><p><small> - Subiu em relação à semana passada </small></p>
  <div class="legenda desceu"></div><p><small> - Desceu em relação à semana passada </small></p>
  <div class="legenda"></div><p><small> - Estável em relação à semana passada </small></p>
  ';
	$retorno_footer .=  '</div>';

    return $retorno_footer;

}

function dah_table($qty,$col,$aside)
{
	$retorno_dah_table = dah_table_header($col,$aside);
	$retorno_dah_table .= dah_table_items($qty,$col);
	$retorno_dah_table .= dah_table_footer($aside);

	return $retorno_dah_table;
}



function render_page($page)
{

	if($page == '')
    {

        $content .= breadcrumb("Home");
        $content .= corpo("Distribuições");
    }

    elseif($page == 'distros_servers')
        {

        $content .= breadcrumb("Linux Servers");
        $content .= corpo_servers("Servers",1);
    }

    elseif($page == 'distros_monitoradas')
        {

        $content .= breadcrumb("Distribuições Monitoradas");
        $content .= corpo_distros("Distribuições Monitoradas",1);
    }

    elseif($page == 'distro_individual')
        {

        //$content = breadcrumb($_GET["distro"]);
        $content .= breadcrumb($_POST["distro"]);
        $content .= corpo_distro_individual($_POST["distro"],1);
    }
    elseif($page == 'loja')
        {

        //$content = breadcrumb($_GET["distro"]);
        $content .= breadcrumb($_POST["loja"]);
        $content .= corpo_loja("Lojinha de distro do Brimo",1);
    }

	return $content;
}


?>
