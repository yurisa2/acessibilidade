<?php

function breadcrumb($titulo_breadcrumb)
{
$return_header = '
<section class="distro">
<!-- Cabeçalho -->
                <div class="row hidden-xs">
                    <div class="col-lg-12">

                        <ol class="breadcrumb">
                            <li>
                                <i class="fa fa-desktop"></i>  <a href="">Distribuições</a>
                            </li>
                            <li class="active">
                                <i class="fa fa-table"></i> '.urldecode($titulo_breadcrumb).'
                            </li>
                        </ol>
                    </div>
                </div>
                <!-- /.row -->';

return $return_header;
}


function painel($n_painel = '')
{


  if($_POST["number_distros"]>0) $distros_server_count = $_POST["number_distros"];
  else   $distros_server_count = rand(20,70);               // Esse é o tipo de cuidado que vc não vê por aí Alberto....pega essa função de persistencia.

  if($_POST["number_distros_loja"]>0) $distros_loja_count = $_POST["number_distros_loja"];
  else   $distros_loja_count = rand(3,10);               // Esse é o tipo de cuidado que vc não vê por aí Alberto....pega essa função de persistencia.

  $painel1 = ' panel-blue'; // Fiz essa atrocidade pq no LAMP funcionou bem, mas no bitnami parecia sujar memória, sei lá.... aí fiz desse jeito aqui....foda-se a vida.
  $painel2 = ' panel-green';
  $painel3 = ' panel-yellow';

  if($n_painel == 1) $painel1 = ''; // Na vdd nem é tão feio, em C++ eu faço direto, chama "método exclusivo"
  if($n_painel == 2) $painel2 = '';
  if($n_painel == 3) $painel3 = '';


  $retorno_painel = '

     <div class="row">
                    <a href="#" onClick="document.getElementById(\'#generico\').submit();" class="none">
                      <div class="col-xs-6 col-sm-6 col-lg-3 col-md-6">
                          <div class="panel'.$painel1.'">
                              <div class="panel-heading">
                                  <div class="row">
                                      <div class="col-xs-3">
                                          <i class="fa fa-television fa-5x"></i>
                                      </div>
                                      <div class="col-xs-9 text-right">
                                          <div class="hidden-xs grande">'.rand(180,600).'</div>
                                          <div class="hidden-xs">Distros Monitoradas</div>
                                      </div>
                                  </div>
                              </div>
                                  <div class="panel-footer">
                                      <div class="pull-left">'.dah_link_generico('distros_monitoradas','Distros','',false).'</div>
                                      <div class="pull-right"><i class="fa fa-arrow-circle-right"></i></div>
                                      <div class="clearfix"></div>
                                  </div>
                          </div>
                    </div>
                    </a>

                    <a href="#" onClick="document.getElementById(\'#servers\').submit();" class="none">
                      <div class="col-xs-6 col-sm-6 col-lg-3 col-md-6">
                          <div class="panel'.$painel2.'">
                              <div class="panel-heading">
                                  <div class="row">
                                      <div class="col-xs-3">
                                          <i class="fa fa-tasks fa-5x"></i>
                                      </div>
                                      <div class="col-xs-9 text-right">
                                          <div class="hidden-xs grande">'.$distros_server_count.'</div>
                                          <div class="hidden-xs">Distros de Servidor</div>
                                      </div>
                                  </div>
                              </div>

                                  <div class="panel-footer">
                                      <div class="pull-left hidden-xs">'.dah_link_servers('distros_servers','Server Distros',$distros_server_count,false,true).'</div>
                                      <div class="pull-left hidden-md hidden-lg hidden-sm pull-left">'.dah_link_servers('distros_servers','Servers',$distros_server_count,false,false).'</div>
                                      <div class="pull-right"><i class="fa fa-arrow-circle-right"></i></div>
                                      <div class="clearfix"></div>
                                  </div>

                          </div>
                      </div>
                    </a>

                    <a href="#" onClick="document.getElementById(\'#loja\').submit();" class="none">
                    <div class="col-xs-6 col-sm-6 col-lg-3 col-md-6">
                        <div class="panel'.$painel3.'">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-shopping-cart fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="hidden-xs grande">'.$distros_loja_count.'</div>
                                        <div class="hidden-xs">Distros comerciais</div>
                                    </div>
                                </div>
                            </div>

                                <div class="panel-footer">
                                    <div class="hidden-xs pull-left">'.dah_link_loja('loja','Lojinha do Brimo',$distros_loja_count,false,true).'</div>
                                    <div class="hidden-md hidden-lg hidden-sm pull-left">'.dah_link_loja('loja','Lojinha',$distros_loja_count,false,false).'</div>
                                    <div class="pull-right"><i class="fa fa-arrow-circle-right"></i></div>
                                    <div class="clearfix"></div>
                                </div>

                        </div>
                    </div>
                    </a>

                    <a href="../aprenda">
                    <div class="col-xs-6 col-sm-6 col-lg-3 col-md-6">
                        <div class="panel panel-red">
                            <div class="panel-heading">
                                <div class="row">
                                    <div class="col-xs-3">
                                        <i class="fa fa-support fa-5x"></i>
                                    </div>
                                    <div class="col-xs-9 text-right">
                                        <div class="hidden-xs grande">'.rand(300,500).'</div>
                                        <div class="hidden-xs">Artigos de ajuda</div>
                                    </div>
                                </div>
                            </div>
                                <div class="panel-footer lbl-ajuda">
                                    <div class="hidden-xs pull-left">Ajuda (portal)</div>
                                      <div class="hidden-md hidden-lg hidden-sm pull-left">Ajuda</div>
                                    <div class="pull-right"><i class="fa fa-arrow-circle-right"></i></div>
                                    <div class="clearfix"></div>
                                </div>
                        </div>
                    </div>
                    </a>
                </div>
                <!-- /.row -->';

    return $retorno_painel;

}



function aside_tabela($numero)
{
    $retorno_aside .= '<aside>';
    $retorno_aside .= dah_table($numero,2,1);
    $retorno_aside .= '</div></aside>';

    return $retorno_aside;

}

function aside_movimentacao()
{
    $retorno_aside = '
                 <aside>
                    <div class="hidden-sm hidden-xs col-md-4 pull-left">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-clock-o fa-fw"></i> Movimentação o L.I.N.U.X </h3>
                            </div>
                            <div class="panel-body">
                                <div class="list-group">
                                    <div class="list-group-item">
                                        <div class="badge">agora</div>
                                        <i class="fa fa-fw fa-calendar"></i> Atualização do '.dah_link_distro().' remarcada
                                    </div>
                                    <div class="list-group-item">
                                        <div class="badge">'.rand(1,20).' minutos atrás</div>
                                        <i class="fa fa-fw fa-comment"></i> Usuário respondeu ao comentário
                                    </div>
                                    <div class="list-group-item">
                                        <div class="badge">'.rand(21,30).' minutos atrás</div>
                                        <i class="fa fa-fw fa-truck"></i> '.rand(50000,90000).' DVDs do Ubuntu enviados
                                    </div>
                                    <div class="list-group-item">
                                        <div class="badge">'.rand(31,59).' minutos atrás</div>
                                        <i class="fa fa-fw fa-money"></i> '.rand(500,900).' Distros compradas hoje
                                    </div>
                                    <div class="list-group-item">
                                        <div class="badge">1 hora atrás</div>
                                        <i class="fa fa-fw fa-user"></i>  '.rand(30,50).' Usuários registrados hoje
                                    </div>
                                    <div class="list-group-item">
                                        <div class="badge">'.rand(2,23).' horas atrás</div>
                                        <i class="fa fa-fw fa-check"></i> '.rand(30,50).' Commits no portal do RBNS
                                    </div>
                                    <div class="list-group-item">
                                        <div class="badge">dois dias atrás</div>
                                        <i class="fa fa-fw fa-check"></i> Finalizando o side bar"
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </aside>
                                       ';
    return $retorno_aside;

}

function corpo($titulo)
{
    $globals['$post'] = $titulo;

    $retorno_conteudo .= '<div class="container">';
    $retorno_conteudo .= '<div class="col-md-8">';
    $retorno_conteudo .= '<img class="img-rounded img-responsive center-block" src="../imgs/imgs-distribuicoes/distros.jpg" alt="distribuicoes">';
    $retorno_conteudo .= '<h2>'.$titulo.'</h2>';
    $retorno_conteudo .= '<p><p>Uma <b>Distribuição Linux</b> (ou simplesmente <i>distro</i>) é um <span class="no-conversion"><b><span lang="pt-pt" xml:lang="pt-pt">sistema operativo</span></b> <sup>(<a href="/wiki/Portugu%C3%AAs_europeu" title="Português europeu">português europeu</a>)</sup> ou <b><span lang="pt-br" xml:lang="pt-br">sistema operacional</span></b> <sup>(<a href="/wiki/Portugu%C3%AAs_brasileiro" title="Português brasileiro">português brasileiro</a>)</sup></span> baseado no <a href="/wiki/N%C3%BAcleo_Linux" class="mw-redirect" title="Núcleo Linux">núcleo Linux</a>, que inclui também um conjunto de software varíavel, um <a href="/wiki/Sistema_gestor_de_pacotes" title="Sistema gestor de pacotes">sistema gestor de pacotes</a> e um <a href="/wiki/Reposit%C3%B3rio" title="Repositório">repositório</a>. Numa típica distribuição Linux, a maior parte do software é <a href="/wiki/Software_livre" title="Software livre">livre</a> e de <a href="/wiki/Software_de_c%C3%B3digo_aberto" title="Software de código aberto">código aberto</a>, estando disponível na forma de pacotes <a href="/wiki/Compila%C3%A7%C3%A3o" class="mw-redirect" title="Compilação">compilados</a> previamente (<a href="/wiki/C%C3%B3digo_de_m%C3%A1quina" title="Código de máquina">binários</a>), e de <a href="/wiki/C%C3%B3digo-fonte" title="Código-fonte">código-fonte</a>. São mantidas por indivíduos como <a href="/wiki/Patrick_Volkerding" title="Patrick Volkerding">Patrick Volkerding</a>, comunidades e projetos, como o <a href="/wiki/Debian_GNU/Linux" class="mw-redirect" title="Debian GNU/Linux">Debian</a> ou o <a href="/wiki/Gentoo_Linux" title="Gentoo Linux">Gentoo</a>. Também podem ser mantidas por grupos e organizações, tais como a <a href="/wiki/Red_Hat" title="Red Hat">Red Hat</a>, a <a href="/wiki/Canonical_Ltd." title="Canonical Ltd.">Canonical</a>, e a <a href="/wiki/Suse" class="mw-redirect" title="Suse">Suse</a>. Os utilizadores de Linux normalmente obtêm o seu sistema operativo descarregando uma das várias distribuições disponíveis, que estão prontas para instalar e utilizar em dispositivos como <a href="/wiki/Computadores_Dom%C3%A9sticos" class="mw-redirect" title="Computadores Domésticos">computadores domésticos</a>, <a href="/wiki/Computadores_port%C3%A1teis" class="mw-redirect" title="Computadores portáteis">portáteis</a>, <a href="/wiki/Servidores" class="mw-redirect" title="Servidores">servidores</a>, <a href="/wiki/Telem%C3%B3veis" class="mw-redirect" title="Telemóveis">telemóveis</a> e outros. Quase todas as distribuições linux são semelhantes ao sistema <a href="/wiki/Unix" title="Unix">Unix</a>. Uma excepção é a distribuição <a href="/wiki/Android" title="Android">Android</a>, que não inclui interface de <a href="/wiki/Linha_de_comandos" title="Linha de comandos">linha de comandos</a> nem software para distribuições linux.</p>
<ul>
<li>Uma Distribuição Linux é composta por uma coleção de aplicativos mais o&#160;<b>kernel</b>&#160;(núcleo) do sistema operacional.</li>
<li>O Linux, na realidade, é apenas o nome do kernel do sistema operacional. Isto significa que todas as distribuições usam o mesmo kernel, mas podem acoplar diversos aplicativos de acordo com o objetivo do seu mantenedor.</li>
<li>O Linux é um sistema operacional "Unix-like", ou seja, tem comportamento similar ao do sistema operacional&#160;<b>Unix</b>&#160;(multi-tarefa e multiusuário).</li>
<li>Uma distribuição Linux pode ser comercial ou não comercial. No primeiro caso, o usuário paga pelo sistema e recebe suporte técnico. No segundo caso, não há qualquer cobrança pelo uso do sistema, basta o usuário fazer o&#160;<i>download</i>&#160;na Internet. Como não há suporte técnico, o usuário deverá tentar resolver os problemas que ocorrerem através das listas de discussão da correspondente distribuição.</li>
</ul>
<p></p></p>';
    $retorno_conteudo .= '</div>';

    $retorno_conteudo .= aside_tabela(20);
    $retorno_conteudo .= '</div></section>'; // Do container

    //$retorno_conteudo .= '<section class="distro">'; // Da outra função ainda. Para validar bonito.

   return $retorno_conteudo;

}



function corpo_distros($titulo,$tipo_aside)
{
    $globals['$post'] = $titulo;

    $retorno_conteudo .= '<div class="container">';
    $retorno_conteudo .= '<div class="col-md-8 pull-right">';
    $retorno_conteudo .= '<h2>'.$titulo.'</h2>';
    $retorno_conteudo .= '<img class="hidden-xs col-md-6 pull-right img-responsive" src="../imgs/imgs-distribuicoes/minisicha-vertical.png" alt="Grafico de utilização de distribuições Linux">';
    $retorno_conteudo .= '<p><p>Existem distribuições com ferramentas para configuração que facilitam a administração do sistema. As principais diferenças entre as distribuições estão nos seus sistemas de pacotes, nas estruturas dos diretórios e na sua biblioteca básica. Por mais que a estrutura dos diretórios siga o mesmo padrão, o <a href="/wiki/FSSTND" class="mw-redirect" title="FSSTND">FSSTND</a> é um padrão muito relaxado, principalmente em arquivos onde as configurações são diferentes entre as distribuições. Então normalmente todos seguem o padrão <a href="/wiki/FHS" class="mw-redirect" title="FHS">FHS</a> (File Hierarchy System), que é o padrão mais novo. Vale lembrar, entretanto, que qualquer aplicativo ou driver desenvolvido para Linux pode ser compilado em qualquer distribuição que vai funcionar da mesma maneira.</p>
<p>Quanto à biblioteca, é usada a Biblioteca <b><a href="/wiki/Libc" class="mw-redirect" title="Libc">libc</a></b>, contendo funções básicas para o sistema Operacional Linux. O problema está quando do lançamento de uma nova versão da Biblioteca libc, algumas das distribuições colocam logo a nova versão, enquanto outras aguardam um pouco. Por isso, alguns programas funcionam numa distribuição e noutras não.</p>
<p><br /></p>
<h2><span class="mw-headline" id="Instala.C3.A7.C3.A3o">Instalação</span><span class="mw-editsection"><span class="mw-editsection-bracket">[</span><a href="/w/index.php?title=Distribui%C3%A7%C3%A3o_Linux&amp;veaction=edit&amp;section=4" class="mw-editsection-visualeditor" title="Editar secção: Instalação"></a><span class="mw-editsection-divider"> | </span><a href="/w/index.php?title=Distribui%C3%A7%C3%A3o_Linux&amp;action=edit&amp;section=4" title="Editar secção: Instalação"></a><span class="mw-editsection-bracket">]</span></span></h2>
<p>Existem muitas maneiras de instalar uma distribuição Linux, a mais comum é gravar um <a href="/wiki/CD" class="mw-redirect" title="CD">CD</a> ou <a href="/wiki/USB_flash_drive" title="USB flash drive">USB flash drive</a> que contenha os programas necessárias para a instalação do sistema base. Estes cds podem ser gravados a partir de ficheiros ISO baixados da internet, comprados on-line, ou parte da capa de revistas sobre informática. Novos utilizadores tendem a particionar o disco de maneira a manter os dados existentes. A nova distribuição Linux pode ser agora instalada na sua partição do disco sem perder os dados existentes.</p>
<p><br />
Existe um movimento LSB (<a href="/wiki/Linux_Standard_Base" title="Linux Standard Base">Linux Standard Base</a>) que proporciona uma maior padronização. Auxilia principalmente vendedores de software que não liberam para distribuição do código fonte, sem tirar características das distribuições.</p>
<h2><span class="mw-headline" id="Exemplos_2">Exemplos</span><span class="mw-editsection"><span class="mw-editsection-bracket">[</span><a href="/w/index.php?title=Distribui%C3%A7%C3%A3o_Linux&amp;veaction=edit&amp;section=5" class="mw-editsection-visualeditor" title="Editar secção: Exemplos"></a><span class="mw-editsection-divider"> | </span><a href="/w/index.php?title=Distribui%C3%A7%C3%A3o_Linux&amp;action=edit&amp;section=5" title="Editar secção: Exemplos"></a><span class="mw-editsection-bracket">]</span></span></h2>
<p>Um exemplo de distribuição que corre num CD é o <a href="/wiki/Kurumin" title="Kurumin">Kurumin</a> <a href="/wiki/Linux" title="Linux">Linux</a>, criado por <a href="/wiki/Carlos_Eduardo_Morimoto" title="Carlos Eduardo Morimoto">Carlos E. Morimoto</a>, baseada no <a href="/wiki/Knoppix" title="Knoppix">Knoppix</a> que em <a rel="nofollow" class="external text" href="http://www.gdhpress.com.br/kurumin/">janeiro de 2008</a> fora descontinuada segundo o seu criador.</p>
<h2 style="cursor: help;" title="Esta seção foi configurada para não ser editável diretamente. Edite a página toda ou a seção anterior em vez disso."><span class="mw-headline" id="Refer.C3.AAncias">Referências</span></h2>
<div class="reflist references-small" style="">
<ol class="references">
<li id="cite_note-1"><span class="mw-cite-backlink"><a href="#cite_ref-1">↑</a></span> <span class="reference-text"><cite class="citation web"><a rel="nofollow" class="external text" href="http://www.slackware.com/announce/1.0.php">«The Slackware Linux Project: Slackware Release Announcement»</a> (em ingles). Slackware.com. 16 de Julho 1993<span class="reference-accessdate">. Consultado em 29 de Julho 2011</span>.</cite><span title="ctx_ver=Z39.88-2004&amp;rfr_id=info%3Asid%2Fpt.wikipedia.org%3ADistribui%C3%A7%C3%A3o+Linux&amp;rft.btitle=The+Slackware+Linux+Project%3A+Slackware+Release+Announcement&amp;rft.date=16+de+Julho+1993&amp;rft.genre=unknown&amp;rft_id=http%3A%2F%2Fwww.slackware.com%2Fannounce%2F1.0.php&amp;rft.pub=Slackware.com&amp;rft_val_fmt=info%3Aofi%2Ffmt%3Akev%3Amtx%3Abook" class="Z3988"><span style="display:none;">&#160;</span></span></span></li>
</ol>
</div></p>';
    $retorno_conteudo .= '</div>';
    $retorno_conteudo .= aside_movimentacao();
    if($tipo_aside == 1) $retorno_conteudo .= dah_table(25,4,0);
    $retorno_conteudo .= '</div>'; // Do container
    $retorno_conteudo .= '</div></section>';

   return $retorno_conteudo;

}

function corpo_servers($titulo,$tipo_aside)
{
    $number_distros = $_POST["number_distros"];

    $retorno_conteudo .= '<div class="container">';
//    $retorno_conteudo .= '<img class="center-block img-responsive" src="../imgs/imgs-distribuicoes/grafico.png" alt="Grafico de utilização de distribuições Linux">';
    $retorno_conteudo .= '<div class="col-md-8 pull-right">';
    $retorno_conteudo .= '<h2>'.$number_distros.' '.$titulo.' Distros em detalhes!</h2>';
    $retorno_conteudo .= '<p><p>Existem distribuições com ferramentas para configuração que facilitam a administração do sistema. As principais diferenças entre as distribuições estão nos seus sistemas de pacotes, nas estruturas dos diretórios e na sua biblioteca básica. Por mais que a estrutura dos diretórios siga o mesmo padrão, o <a href="/wiki/FSSTND" class="mw-redirect" title="FSSTND">FSSTND</a> é um padrão muito relaxado, principalmente em arquivos onde as configurações são diferentes entre as distribuições. Então normalmente todos seguem o padrão <a href="/wiki/FHS" class="mw-redirect" title="FHS">FHS</a> (File Hierarchy System), que é o padrão mais novo. Vale lembrar, entretanto, que qualquer aplicativo ou driver desenvolvido para Linux pode ser compilado em qualquer distribuição que vai funcionar da mesma maneira.</p>
<p>Quanto à biblioteca, é usada a Biblioteca <b><a href="/wiki/Libc" class="mw-redirect" title="Libc">libc</a></b>, contendo funções básicas para o sistema Operacional Linux. O problema está quando do lançamento de uma nova versão da Biblioteca libc, algumas das distribuições colocam logo a nova versão, enquanto outras aguardam um pouco. Por isso, alguns programas funcionam numa distribuição e noutras não.</p>
<p><br /></p>
<h2><span class="mw-headline" id="Instala.C3.A7.C3.A3o">Instalação</span><span class="mw-editsection"><span class="mw-editsection-bracket">[</span><a href="/w/index.php?title=Distribui%C3%A7%C3%A3o_Linux&amp;veaction=edit&amp;section=4" class="mw-editsection-visualeditor" title="Editar secção: Instalação"></a><span class="mw-editsection-divider"> | </span><a href="/w/index.php?title=Distribui%C3%A7%C3%A3o_Linux&amp;action=edit&amp;section=4" title="Editar secção: Instalação"></a><span class="mw-editsection-bracket">]</span></span></h2>
<p>Existem muitas maneiras de instalar uma distribuição Linux, a mais comum é gravar um <a href="/wiki/CD" class="mw-redirect" title="CD">CD</a> ou <a href="/wiki/USB_flash_drive" title="USB flash drive">USB flash drive</a> que contenha os programas necessárias para a instalação do sistema base. Estes cds podem ser gravados a partir de ficheiros ISO baixados da internet, comprados on-line, ou parte da capa de revistas sobre informática. Novos utilizadores tendem a particionar o disco de maneira a manter os dados existentes. A nova distribuição Linux pode ser agora instalada na sua partição do disco sem perder os dados existentes.</p>
<p><br />
Existe um movimento LSB (<a href="/wiki/Linux_Standard_Base" title="Linux Standard Base">Linux Standard Base</a>) que proporciona uma maior padronização. Auxilia principalmente vendedores de software que não liberam para distribuição do código fonte, sem tirar características das distribuições.</p>
<h2><span class="mw-headline" id="Exemplos_2">Exemplos</span><span class="mw-editsection"><span class="mw-editsection-bracket">[</span><a href="/w/index.php?title=Distribui%C3%A7%C3%A3o_Linux&amp;veaction=edit&amp;section=5" class="mw-editsection-visualeditor" title="Editar secção: Exemplos"></a><span class="mw-editsection-divider"> | </span><a href="/w/index.php?title=Distribui%C3%A7%C3%A3o_Linux&amp;action=edit&amp;section=5" title="Editar secção: Exemplos"></a><span class="mw-editsection-bracket">]</span></span></h2>
<p>Um exemplo de distribuição que corre num CD é o <a href="/wiki/Kurumin" title="Kurumin">Kurumin</a> <a href="/wiki/Linux" title="Linux">Linux</a>, criado por <a href="/wiki/Carlos_Eduardo_Morimoto" title="Carlos Eduardo Morimoto">Carlos E. Morimoto</a>, baseada no <a href="/wiki/Knoppix" title="Knoppix">Knoppix</a> que em <a rel="nofollow" class="external text" href="http://www.gdhpress.com.br/kurumin/">janeiro de 2008</a> fora descontinuada segundo o seu criador.</p>
<h2 style="cursor: help;" title="Esta seção foi configurada para não ser editável diretamente. Edite a página toda ou a seção anterior em vez disso."><span class="mw-headline" id="Refer.C3.AAncias">Referências</span></h2>
<div class="reflist references-small" style="">
<ol class="references">
<li id="cite_note-1"><span class="mw-cite-backlink"><a href="#cite_ref-1">↑</a></span> <span class="reference-text"><cite class="citation web"><a rel="nofollow" class="external text" href="http://www.slackware.com/announce/1.0.php">«The Slackware Linux Project: Slackware Release Announcement»</a> (em ingles). Slackware.com. 16 de Julho 1993<span class="reference-accessdate">. Consultado em 29 de Julho 2011</span>.</cite><span title="ctx_ver=Z39.88-2004&amp;rfr_id=info%3Asid%2Fpt.wikipedia.org%3ADistribui%C3%A7%C3%A3o+Linux&amp;rft.btitle=The+Slackware+Linux+Project%3A+Slackware+Release+Announcement&amp;rft.date=16+de+Julho+1993&amp;rft.genre=unknown&amp;rft_id=http%3A%2F%2Fwww.slackware.com%2Fannounce%2F1.0.php&amp;rft.pub=Slackware.com&amp;rft_val_fmt=info%3Aofi%2Ffmt%3Akev%3Amtx%3Abook" class="Z3988"><span style="display:none;">&#160;</span></span></span></li>
</ol>
</div></p>';
    $retorno_conteudo .= '<img class="img-responsive" src="../imgs/imgs-distribuicoes/serverfarm.png" alt="Tux! Bitches!">';
    $retorno_conteudo .= '<ol type="1" style="list-style: decimal inside;">';

    $retorno_conteudo .= '<li><div>'.dah_link_distro('',true,false).' - '.dah_lipsum(70).'</div></li>';

        for($i = 2; $i <= $number_distros; $i++)
        {
            $retorno_conteudo .= '<li><div>'.dah_link_distro('',true,false).' - '.dah_lipsum(70).'</div></li>';
        }
    $retorno_conteudo .= '</ol></div>';
    $retorno_conteudo .= aside_tabela($number_distros);
    $retorno_conteudo .= '</div></section>'; // Do container

   return $retorno_conteudo;

}

function corpo_loja($titulo,$tipo_aside)
{
    $number_distros = $_POST["number_distros_loja"];
    $array_panels = array("panel-yellow","panel-red","panel-green","panel-blue");
    $array_pay_type = array("credit-card","cc-discover","paypal");
    $retorno_conteudo .= '<div class="container">';
    $retorno_conteudo .= '<div class="col-md-8 pull-right">';
    $retorno_conteudo .= '<h1>'.$titulo.'</h1>';
    $retorno_conteudo .= '<p>'.dah_lipsum().'</p>';

    for($i = 1; $i <= $number_distros; $i++)
    {
      $distro_form = dah_distro();
    $color_panel = $array_panels[array_rand($array_panels)];
    $pay_type =    $array_pay_type[array_rand($array_pay_type)];
    $retorno_conteudo .= '

    <a href="# " onClick="document.getElementById(\'#'.$distro_form.'\').submit();">
      <div class="col-xs-6 col-lg-4 col-md-4">
          <div class="panel '.$color_panel.'">
              <div class="panel-heading">
                  <div class="row">
                      <div class="hidden-xs col-sm-3 pull-left">
                          <i class="fa fa-linux fa-3x"></i>
                      </div>
                      <div class="col-xs-3 text-center">
                          <i class="fa fa-shopping-cart fa-4x"></i>
                      </div>
                      <div class="hidden-xs col-sm-3 pull-right">
                          <i class="fa fa-inverse fa-'.$pay_type.' fa-2x"></i>
                      </div>
                  </div>
              </div>


                  <div class="panel-footer">
                      <div>'.dah_link_distro($distro_form,false).'</div>
                      <div class="pull-right"><i class="fa fa-dollar"></i><b>'.rand(10,99).'9,99</b><small>/ano</small></div>
                      <div class="clearfix"></div>
                  </div>

          </div>
      </div>
      </a>';
    }
     $retorno_conteudo .= '
    <i class="fa fa-paypal fa-2x" aria-hidden="true" ></i> - PayPal <br>
    <i class="fa fa-credit-card fa-2x" aria-hidden="true"></i> - Visa + Master <br>
    <i class="fa fa-cc-discover fa-2x" aria-hidden="true"></i> - Visa + Master + Discover Card <br>
    ';

    $retorno_conteudo .= '</div>';

    $retorno_conteudo .= aside_tabela($number_distros);
    $retorno_conteudo .= '</div></section>'; // Do container

   return $retorno_conteudo;

}

function corpo_distro_individual($titulo,$tipo_aside)
{
    $globals['$post'] = $titulo;

    $distro = $_POST["distro"];
    $retorno_conteudo .= '<div class="container">';
    $retorno_conteudo .= '<div class="col-xs-12 col-md-8 pull-right">';
    $retorno_conteudo .= '<h1>'.$titulo.'</h1>';
    $retorno_conteudo .= '<img class="img-responsive col-xs-12 col-sm-5" src="../imgs/imgs-distribuicoes/tux'.rand(1,7).'.png" alt="Tux! Bitches!">';
    $retorno_conteudo .= '<p><p>Existem distribuições com ferramentas para configuração que facilitam a administração do sistema. As principais diferenças entre as distribuições estão nos seus sistemas de pacotes, nas estruturas dos diretórios e na sua biblioteca básica. Por mais que a estrutura dos diretórios siga o mesmo padrão, o <a href="/wiki/FSSTND" class="mw-redirect" title="FSSTND">FSSTND</a> é um padrão muito relaxado, principalmente em arquivos onde as configurações são diferentes entre as distribuições. Então normalmente todos seguem o padrão <a href="/wiki/FHS" class="mw-redirect" title="FHS">FHS</a> (File Hierarchy System), que é o padrão mais novo. Vale lembrar, entretanto, que qualquer aplicativo ou driver desenvolvido para Linux pode ser compilado em qualquer distribuição que vai funcionar da mesma maneira.</p>
<p>Quanto à biblioteca, é usada a Biblioteca <b><a href="/wiki/Libc" class="mw-redirect" title="Libc">libc</a></b>, contendo funções básicas para o sistema Operacional Linux. O problema está quando do lançamento de uma nova versão da Biblioteca libc, algumas das distribuições colocam logo a nova versão, enquanto outras aguardam um pouco. Por isso, alguns programas funcionam numa distribuição e noutras não.</p>
<p><br /></p>
<h2><span class="mw-headline" id="Instala.C3.A7.C3.A3o">Instalação</span><span class="mw-editsection"><span class="mw-editsection-bracket">[</span><a href="/w/index.php?title=Distribui%C3%A7%C3%A3o_Linux&amp;veaction=edit&amp;section=4" class="mw-editsection-visualeditor" title="Editar secção: Instalação"></a><span class="mw-editsection-divider"> | </span><a href="/w/index.php?title=Distribui%C3%A7%C3%A3o_Linux&amp;action=edit&amp;section=4" title="Editar secção: Instalação"></a><span class="mw-editsection-bracket">]</span></span></h2>
<p>Existem muitas maneiras de instalar uma distribuição Linux, a mais comum é gravar um <a href="/wiki/CD" class="mw-redirect" title="CD">CD</a> ou <a href="/wiki/USB_flash_drive" title="USB flash drive">USB flash drive</a> que contenha os programas necessárias para a instalação do sistema base. Estes cds podem ser gravados a partir de ficheiros ISO baixados da internet, comprados on-line, ou parte da capa de revistas sobre informática. Novos utilizadores tendem a particionar o disco de maneira a manter os dados existentes. A nova distribuição Linux pode ser agora instalada na sua partição do disco sem perder os dados existentes.</p>
<p><br />
Existe um movimento LSB (<a href="/wiki/Linux_Standard_Base" title="Linux Standard Base">Linux Standard Base</a>) que proporciona uma maior padronização. Auxilia principalmente vendedores de software que não liberam para distribuição do código fonte, sem tirar características das distribuições.</p>
<h2><span class="mw-headline" id="Exemplos_2">Exemplos</span><span class="mw-editsection"><span class="mw-editsection-bracket">[</span><a href="/w/index.php?title=Distribui%C3%A7%C3%A3o_Linux&amp;veaction=edit&amp;section=5" class="mw-editsection-visualeditor" title="Editar secção: Exemplos"></a><span class="mw-editsection-divider"> | </span><a href="/w/index.php?title=Distribui%C3%A7%C3%A3o_Linux&amp;action=edit&amp;section=5" title="Editar secção: Exemplos"></a><span class="mw-editsection-bracket">]</span></span></h2>
<p>Um exemplo de distribuição que corre num CD é o <a href="/wiki/Kurumin" title="Kurumin">Kurumin</a> <a href="/wiki/Linux" title="Linux">Linux</a>, criado por <a href="/wiki/Carlos_Eduardo_Morimoto" title="Carlos Eduardo Morimoto">Carlos E. Morimoto</a>, baseada no <a href="/wiki/Knoppix" title="Knoppix">Knoppix</a> que em <a rel="nofollow" class="external text" href="http://www.gdhpress.com.br/kurumin/">janeiro de 2008</a> fora descontinuada segundo o seu criador.</p>
<h2 style="cursor: help;" title="Esta seção foi configurada para não ser editável diretamente. Edite a página toda ou a seção anterior em vez disso."><span class="mw-headline" id="Refer.C3.AAncias">Referências</span></h2>
<div class="reflist references-small" style="">
<ol class="references">
<li id="cite_note-1"><span class="mw-cite-backlink"><a href="#cite_ref-1">↑</a></span> <span class="reference-text"><cite class="citation web"><a rel="nofollow" class="external text" href="http://www.slackware.com/announce/1.0.php">«The Slackware Linux Project: Slackware Release Announcement»</a> (em ingles). Slackware.com. 16 de Julho 1993<span class="reference-accessdate">. Consultado em 29 de Julho 2011</span>.</cite><span title="ctx_ver=Z39.88-2004&amp;rfr_id=info%3Asid%2Fpt.wikipedia.org%3ADistribui%C3%A7%C3%A3o+Linux&amp;rft.btitle=The+Slackware+Linux+Project%3A+Slackware+Release+Announcement&amp;rft.date=16+de+Julho+1993&amp;rft.genre=unknown&amp;rft_id=http%3A%2F%2Fwww.slackware.com%2Fannounce%2F1.0.php&amp;rft.pub=Slackware.com&amp;rft_val_fmt=info%3Aofi%2Ffmt%3Akev%3Amtx%3Abook" class="Z3988"><span style="display:none;">&#160;</span></span></span></li>
</ol>
</div></p>';
    $retorno_conteudo .= '</div>';
    $retorno_conteudo .= aside_movimentacao();
    $retorno_conteudo .= '</div></section>'; // Do container

   return $retorno_conteudo;

}






?>
